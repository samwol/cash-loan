# 现金贷

小额现金贷网络贷款系统源码 可打包成APP

#### 软件架构
软件架构说明


#### 安装教程

1.将源码上传到根目录并且解压。

2.导入数据库文件“cash-loan.sql”到你的数据库

3.修改域名配置文件：“/App/Conf/domain.php”

<?php
return array(

	'APP_SUB_DOMAIN_DEPLOY'=>1,

      
        'APP_SUB_DOMAIN_RULES'=>array(
   
           'www.2.com'=>array('Manage/'),

           'www.1.com'=>array('Wchat/'),

    ),

);

这一步很重要，把域名换成你的域名，你域名需要解析到这个根目录，不用解析到文件夹，只要解析到根目录，正常解析就行。

4.修改数据库配置文件：“/App/Conf/database.php”

5.打开后台：/www.2.com（这里ww.2.com就是你之前解析的域名）

6.账户：admin           密码：123456



