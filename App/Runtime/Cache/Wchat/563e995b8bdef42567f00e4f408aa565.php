<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/bankCss.css" rel="stylesheet">
		<link type="text/css" href="__PUBLIC__/Wchat/css/LArea.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/one_LArea.js"></script>
		<script src="__PUBLIC__/Wchat/js/LAreaData2.js"></script>
		<title>联系人信息 - 信息认证 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
				<section class="ioc_list">
			<ul>
				<li class="col-xs-7 identityAuth"></li>
				<li class="col-xs-7 contactsAuth"></li>
				<li class="col-xs-7 bankAuth"></li>
				<li class="col-xs-7 addessAuth"></li>
				<li class="col-xs-7 mobileAuth"></li>
				<li class="col-xs-7 taobaoAuth"></li>
			</ul>
		</section>
		<?php $actionName = ACTION_NAME; ?>
		<script>
			<?php if(is_array($auth)): $i = 0; $__LIST__ = $auth;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(!empty($vo)): ?>var obj = $(".ioc_list li."+"<?php echo ($key); ?>"+"Auth");
					obj.removeClass("<?php echo ($key); ?>"+"Auth");
					obj.addClass("<?php echo ($key); ?>"+"AuthNow");
					obj.html("<span></span>");<?php endif; endforeach; endif; else: echo "" ;endif; ?>
			var actionName = "<?php echo ($actionName); ?>";
			var obj = $(".ioc_list li."+actionName);
			obj.removeClass(actionName);
			obj.addClass(actionName+"Now");
		</script>
		<form action="<?php echo U('Info/contactsAuth');?>" method="post">
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12 hang">
						<label>直系亲属关系</label>
						<span class="form-control duan" id="zhishu">请选择</span>
						<input type="hidden" id="zhishuRelation" name="zhishuRelation">
					</li>
					<li class="col-xs-12">
						<label>直系亲属姓名</label>
						<input type="text" placeholder="请输入" class="duan" name="zhishuName">
					</li>
					<li class="col-xs-12 hang1">
						<label>直系亲属电话</label>
						<input type="text" placeholder="请输入" class="duan dianhua" name="zhishuPhone">
					</li>
				</ul>
			</div>
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12 hang">
						<label>紧急联系人关系</label>
						<span class="form-control duan" id="jinji">请选择</span>
						<input type="hidden" id="jinjiRelation" name="jinjiRelation">
					</li>
					<li class="col-xs-12">
						<label>紧急联系人姓名</label>
						<input type="text" placeholder="请输入" class="duan" name="jinjiName">
					</li>
					<li class="col-xs-12 hang1">
						<label>紧急联系人电话</label>
						<input type="text" placeholder="请输入" class="duan dianhua" name="jinjiPhone">
					</li>
				</ul>
			</div>
		</form>
		<div class="footer">
			<button class="but1" id="nextBtn">下一步</button>
		</div>
	</body>
	<script>
		var area3 = new one_LArea();
		area3.init({
			'trigger': '#zhishu',
			'valueTo': '#zhishuRelation',
			'keys': {
				id: 'value',
				name: 'text'
			},
			'type': 2,
			'data': [guanxi_zhishu]
		});
		var area2 = new one_LArea();
		area2.init({
			'trigger': '#jinji',
			'valueTo': '#jinjiRelation',
			'keys': {
				id: 'value',
				name: 'text'
			},
			'type': 2,
			'data': [guanxi]
		});
		$(function(){
			$("#nextBtn").on('click',function(){
				var zhishuRelation = $("input[name='zhishuRelation']").val();
				var zhishuName = $("input[name='zhishuName']").val();
				var zhishuPhone = $("input[name='zhishuPhone']").val();
				var jinjiRelation = $("input[name='jinjiRelation']").val();
				var jinjiName = $("input[name='jinjiName']").val();
				var jinjiPhone = $("input[name='jinjiPhone']").val();
				if(zhishuRelation.length == 0){
					cvphp.msg({
	    				content: '请选择直系亲属关系'
	    			});
	    			return ;
				}
				if(zhishuName.length == 0){
					cvphp.msg({
	    				content: '请输入直系亲属姓名'
	    			});
	    			return ;
				}
				if(!cvphp.ismobile(zhishuPhone)){
					cvphp.msg({
	    				content: '请输入规范的手机号'
	    			});
	    			return ;
				}
				if(jinjiRelation.length == 0){
					cvphp.msg({
	    				content: '请选择直系亲属关系'
	    			});
	    			return ;
				}
				if(jinjiName.length == 0){
					cvphp.msg({
	    				content: '请输入直系亲属姓名'
	    			});
	    			return ;
				}
				if(!cvphp.ismobile(jinjiPhone)){
					cvphp.msg({
	    				content: '请输入规范的手机号'
	    			});
	    			return ;
				}
				cvphp.submit($("form"),function(data){
					if(data.status != 1){
						cvphp.msg({
		    				content: data.info
		    			});
						return false;
					}else{
						cvphp.msg({
		    				content: '保存成功'
		    			});
		    			var url = data.url;
		    			if(url.length > 0){
		    				setTimeout(function(){
		    					window.location.href = url;
		    				},2000);
		    			}
					}
				});
			});
		});
	</script>
</html>