<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Wchat/css/Current.css">
		<title>还款计划 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>  - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
		<?php $nowBill = $data['nowBill']; ?>
		<div class="dangqi">
			<span><?php echo ($data["oid"]); ?></span>
			<p>当期应还</p>
			<h2>￥<?php echo ($nowBill["money"]); ?></h2>
			<p>借款：￥<?php echo ($data["money"]); ?></p>
			
<?php if($nowBill['status'] == 0): ?><label>待还款</label><?php endif; ?>
<?php if($nowBill['status'] == 1): ?><label>已逾期</label><?php endif; ?>
<?php if($nowBill['status'] == 2): ?><label>已还清</label><?php endif; ?>
<?php if($nowBill['status'] == 3): ?><label>逾期还清</label><?php endif; ?>
<?php if($nowBill['status'] == 4): ?><label>账单失效</label><?php endif; ?>
		</div>
		<div class="mun">
			<div class="anniu">
				<a href="<?php echo U('Repay/repayment',array('id'=>$nowBill['id']));?>">
					<h4>当期应还</h4>
					<em>￥<?php echo ($nowBill["money"]); ?></em>
				</a>
				<a href="#" class="jiesuan">
					<h4>剩余结清</h4>
					<em>￥<?php echo ($data["allBillMoney"]); ?></em>
				</a>
			</div>
			<div class="row list">
				<div class="title">
<?php if($data['timetype'] == 1): ?><span>共<?php echo ($data["time"]); ?>期</span>
	<?php else: ?>
					<span>共1期</span><?php endif; ?>
					<strong>剩余未还款：￥<?php echo ($data["allBillMoney"]); ?></strong>
				</div>
				<div class="hk_list">
					<ul>
<?php $billList = $data['billList']; ?>
<?php if(is_array($billList)): $i = 0; $__LIST__ = $billList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo['status'] == 0): ?><li class="col-xs-12">
		<?php elseif($vo['status'] == 1): ?>
						<li class="col-xs-12 yuqi">
		<?php else: ?>
						<li class="col-xs-12 huanqing"><?php endif; ?>
							<div class="xiao">
								<span><?php echo (date("Y/m/d",$vo["repayment_time"])); ?></span>
								<em>第<?php echo ($vo["billnum"]); ?>期</em>
							</div>
							<div class="xiao1">
								<span>￥<?php echo ($vo["allmoney"]); ?></span>
							</div>
							<div class="xiao2">
	<?php if($vo['status'] == 0): ?><span>待还款</span><?php endif; ?>
	<?php if($vo['status'] == 1): ?><span>已逾期</span><?php endif; ?>
	<?php if($vo['status'] == 2): ?><span>已还清</span><?php endif; ?>
	<?php if($vo['status'] == 3): ?><span>逾期还清</span><?php endif; ?>
	<?php if($vo['status'] == 4): ?><span>账单失效</span><?php endif; ?>
							</div>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>