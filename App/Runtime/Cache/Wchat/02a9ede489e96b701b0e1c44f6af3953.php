<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/bankCss.css" rel="stylesheet">
		<title>淘宝验证- 信息认证 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<style>
		.yanzheng{
		text-align: center;
		position: absolute;
		top: 45%;
		left: 50%;
		margin-left: -70px;
		}
	</style>
	<body>
				<section class="ioc_list">
			<ul>
				<li class="col-xs-7 identityAuth"></li>
				<li class="col-xs-7 contactsAuth"></li>
				<li class="col-xs-7 bankAuth"></li>
				<li class="col-xs-7 addessAuth"></li>
				<li class="col-xs-7 mobileAuth"></li>
				<li class="col-xs-7 taobaoAuth"></li>
			</ul>
		</section>
		<?php $actionName = ACTION_NAME; ?>
		<script>
			<?php if(is_array($auth)): $i = 0; $__LIST__ = $auth;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(!empty($vo)): ?>var obj = $(".ioc_list li."+"<?php echo ($key); ?>"+"Auth");
					obj.removeClass("<?php echo ($key); ?>"+"Auth");
					obj.addClass("<?php echo ($key); ?>"+"AuthNow");
					obj.html("<span></span>");<?php endif; endforeach; endif; else: echo "" ;endif; ?>
			var actionName = "<?php echo ($actionName); ?>";
			var obj = $(".ioc_list li."+actionName);
			obj.removeClass(actionName);
			obj.addClass(actionName+"Now");
		</script>
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12">
						<label>淘宝帐号</label>
						<input type="text" placeholder="请输入手机号" class="form-control" name="mobile" />
					</li>
					<li class="col-xs-12">
						<label>淘宝密码</label>
						<input type="text" placeholder="请输入服务密码" class="form-control" name="fwpass" />
					</li>
					<li class="col-xs-12">
						<button type="button" class="btn btn-primary" id="query" onclick="TaoBaoAuth()">确认授权</button>
					</li>
				</ul>
				<button type="button" class="btn btn-primary" style="display:none;text-align:center;"  id="query">点击淘宝授权</button>
			</div>
			<div class="yanzheng" style="text-align:center;display:none;">
						<img src=""/>	
						<p style="text-align:center;">打开淘宝扫码</p>
			</div>
		<div class="footer">
			<button class="but1" id="next">跳过</button>
		</div>
	</body>
	<script>
		/*function resend(){
			if(resendTime == 1){
				$("#yzm").html('重新发送');
				clearInterval(resendFun);
				$("#yzm").removeAttr('disabled');
			}else{
				resendTime--;
				$("#yzm").html(resendTime + ' 秒重试');
				$("#yzm").attr('disabled',"true");
			}
		}*/
		function TaoBaoAuth(){
            var mobile = $("input[name='mobile']").val();
            var fwpass = $("input[name='fwpass']").val();
            if(mobile.length == 0){
                cvphp.msg({
                    content: '请输入手机号'
                });
                return false;
            }
            if(fwpass.length == 0){
                cvphp.msg({
                    content: '请输入服务密码'
                });
                return false;
            }
            if(!cvphp.ismobile(mobile)){
                cvphp.msg({
                    content: '请输入规范的手机号'
                });
                return false;
            }
            layer.open({
                type: 2
                ,content: '读取中请勿退出'
            });
            setTimeout(function(){
                layer.closeAll();
                cvphp.mpost(
                    "<?php echo U('Info/next');?>",
                    {
                        name:'taobao',
                    },
                    function(data){
                        if(data.status != 1){
                            // cvphp.msg({
                            //     content: data.info
                            // });
                            console.log(data);
                        }else{
                            alert("授权成功");
                            //window.location.reload();
                            location.href="<?php echo U('Index/index');?>";
                        }
                    }
                );


			},8000)
		}

		var $token = '';
		var time = 1;
		var rw = null;
		function getstatus(token){
				rw = setInterval(function(){
				cvphp.mpost("<?php echo U('Info/taobaostatus');?>",{token:$token},function(data){
					console.log(data.msg);
					if(data.code == '0006' && data.input.type == 'qr'){
						$(".yanzheng img").attr('src',"data:image/png;base64,"+data.input.value); 
						$(".yanzheng").show();
						//clearInterval(rw);
					}else if(data.code == '0000'){
						cvphp.mpost(
							"<?php echo U('Info/taobaoAuth');?>",
							{
								token:$token
							},
							function(data){
								layer.closeAll();
								if(data.status != 1){
									//$token = data.token;
									//getstatus($token);
									cvphp.msg({
										content: data.info
									});
									console.log(data);
								}else{
									cvphp.msg({
										content: "授权成功"
									});
									clearInterval(rw);
									location.href="<?php echo U('Index/index');?>";
								}
							}
						);	
						clearInterval(rw);						
					}else if(data.code == '1110'){
							cvphp.msg({
								content: data.msg
							});	
							clearInterval(rw);	
							$("#query").trigger('click');
					}else if(data.code == '0100'){
						  layer.open({
							type: 2
							,content: '读取中请勿退出'
						  });
					}
				});
				time ++;
				if(time >= 200){
					cvphp.msg({
						content: "授权超时"
					});
					clearInterval(rw);
				}
			},3000);			
		}
		$(function(){
			$("#query").click(function(){		
				cvphp.mpost(
					"<?php echo U('Info/taobao');?>",
					{
						mobile:1
					},
					function(data){
						if(data.code == "0010"){
							$token = data.token;
							getstatus($token);
						}else{
							// cvphp.msg({
							// 	content:data.msg
							//
							// })
							console.log(data.msg);
						}
					}
				);
			
			});	
			$("#next").on('click',function(){
				cvphp.mpost(
					"<?php echo U('Info/next');?>",
					{
						name:'taobao',						
					},
					function(data){
							if(data.status != 1){
								cvphp.msg({
									content: data.info
								});
								console.log(data);
							}else{
								//window.location.reload();
								location.href="<?php echo U('Index/index');?>";
							}
					}
				);
				//location.href="<?php echo U('Info/taobaoAuth');?>";
			});	
			// $("#query").trigger('click');
		});
	</script>
</html>