<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/bankCss.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/LArea.js"></script>
		<link type="text/css" href="__PUBLIC__/Wchat/css/LArea.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/one_LArea.js"></script>
		<script src="__PUBLIC__/Wchat/js/LAreaData2.js"></script>
		<title>填写个人信息 - 信息认证 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
				<section class="ioc_list">
			<ul>
				<li class="col-xs-7 identityAuth"></li>
				<li class="col-xs-7 contactsAuth"></li>
				<li class="col-xs-7 bankAuth"></li>
				<li class="col-xs-7 addessAuth"></li>
				<li class="col-xs-7 mobileAuth"></li>
				<li class="col-xs-7 taobaoAuth"></li>
			</ul>
		</section>
		<?php $actionName = ACTION_NAME; ?>
		<script>
			<?php if(is_array($auth)): $i = 0; $__LIST__ = $auth;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(!empty($vo)): ?>var obj = $(".ioc_list li."+"<?php echo ($key); ?>"+"Auth");
					obj.removeClass("<?php echo ($key); ?>"+"Auth");
					obj.addClass("<?php echo ($key); ?>"+"AuthNow");
					obj.html("<span></span>");<?php endif; endforeach; endif; else: echo "" ;endif; ?>
			var actionName = "<?php echo ($actionName); ?>";
			var obj = $(".ioc_list li."+actionName);
			obj.removeClass(actionName);
			obj.addClass(actionName+"Now");
		</script>
		<form action="<?php echo U('Info/addessAuth');?>" method="post">
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12 hang">
						<label>婚姻状况</label>
						<span class="form-control" id="marriageSel">请选择婚姻状况</span>
						<input type="hidden" id="marriage" name="marriage" />
					</li>
					<li class="col-xs-12 hang">
						<label>学历</label>
						<span class="form-control" id="educationSel">请选择您的最高学历</span>
						<input type="hidden" id="education" name="education" />
					</li>
					<li class="col-xs-12">
						<label>从事行业</label>
						<input type="text" placeholder="请输入您的工作行业" class="form-control" name="industry" />
					</li>
				</ul>
			</div>
			<p class="changzhu">常住地址</p>
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12 hang">
						<label>居住地址</label>
						<span class="form-control" id="addessSel">请选择居住城市</span>
						<input type="hidden" id="addess" name="addess" />
					</li>
					<li class="col-xs-12">
						<label>详细地址</label>
						<input type="text" placeholder="请输入详细地址具体到门牌号" class="form-control" name="addessMore" />
					</li>
				</ul>
			</div>
		</form>
		<div class="footer">
			<button class="but1" id="nextBtn">下一步</button>
		</div>
	</body>
	<script>
		$(function(){
			$("#nextBtn").on('click',function(){
				cvphp.submit($("form"),function(data){
					if(data.status != 1){
						cvphp.msg({
		    				content: data.info
		    			});
						return false;
					}else{
						cvphp.msg({
		    				content: '保存成功'
		    			});
		    			var url = data.url;
		    			if(url.length > 0){
		    				setTimeout(function(){
		    					window.location.href = url;
		    				},2000);
		    			}
					}
				});
			});
			
			
		});
		
		var area3 = new one_LArea();
		area3.init({
			'trigger': '#marriageSel',
			'valueTo': '#marriage',
			'keys': {
				id: 'value',
				name: 'text'
			},
			'type': 2,
			'data': [marriage]
		});
		var area2 = new one_LArea();
		area2.init({
			'trigger': '#educationSel',
			'valueTo': '#education',
			'keys': {
				id: 'value',
				name: 'text'
			},
			'type': 2,
			'data': [education]
		});
		var area4 = new LArea();
		area4.init({
			'trigger': '#addessSel',
			'valueTo': '#addess',
			'keys': {
				id: 'value',
				name: 'text'
			},
			'type': 2,
			'data': [provs_data, citys_data, dists_data]
		});
	</script>
</html>