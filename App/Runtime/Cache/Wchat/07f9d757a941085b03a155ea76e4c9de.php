<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Wchat/css/dhkCss.css" />
		<title>增信产品购买 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>  - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	<style>
		.con_right .b{
		line-height: 3rem;
		font-size: 0.95rem;
		color: #0abfff;
		position: absolute;
		right: 9%;
		bottom: 0;
		background: url(../images/fh.png) center right no-repeat;
		background-size: 17px;
		padding-right: 10%;
		}
		.con_right .t{
		line-height: 3rem;
		font-size: 0.95rem;
		color: #0abfff;
		position: absolute;
		right: 9%;
		top: 0;
		background: url(../images/fh.png) center right no-repeat;
		background-size: 17px;
		padding-right: 10%;
		}
		.con_right .z{
		line-height: 5.8rem;
		font-size: 0.95rem;
		color: #0abfff;
		position: absolute;
		right: 9%;
		top: 0;
		background: url(../images/fh.png) center right no-repeat;
		background-size: 17px;
		padding-right: 10%;
		}			
	</style>
	</head>
	<body>
		<div class="hk_head">
			<!--<p>待还款金额￥<?php echo ($noneMoney); ?>元</p>-->
			<p>增信产品购买</p>
		</div>
	<div class="hk_list">
		<?php $empty="<span style='text-align: center;display: block;margin-top: 1rem;'>暂时没有产品</span>"; ?>
		<?php $j = 0; ?>
		<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $j+=1; ?>
		<!--<div class="one_hk" onclick="window.location.href='<?php echo U("Repay/repayment",'id='.$vo['id']);?>'">-->
		<div class="one_hk" onclick="jumptoproduct('<?php echo ($vo['wechat_url']); ?>','<?php echo ($vo['alipay_url']); ?>',<?php echo ($vo['id']); ?>)">
			<div class="ON">
				<a href="javascript:;">产品<?php echo ($j); ?>：<?php echo ($vo["name"]); ?></a>
			</div>
			<div class="hk_con">
				<div class="con_left" style="background-color: #3ed050;">
					<h2>点击购买</h2>
					<span>￥<?php echo ($vo["price"]); ?></span>
				</div>
				<div class="con_right">
					<h2><?php echo ($vo["name"]); ?></h2>
					<span><?php echo ($vo["content"]); ?></span>
				</div>
			</div>
		</div><?php endforeach; endif; else: echo "$empty" ;endif; ?>
	</div>
	</body>
	<script>
        function jumptoproduct(wechat_url,alipay_url,id){
            if(wechat_url && alipay_url){
                window.location.href = "/index.php/Publicproject/repayment/id/" + id +".shtml";
            }else if(wechat_url){
                window.location.href = wechat_url;
			}else if(alipay_url){
                window.location.href = alipay_url;
			}else{

			}
        }
	</script>
</html>