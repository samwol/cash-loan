<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/bankCss.css" rel="stylesheet">
		<title>购买套餐 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
		<form action="<?php echo U('Repay/buy');?>" id="form1" method="post">
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12">
						<select name="total_fee"id="total_fee">
						    <option value="">请选择金额</option>
						    <option value="588">588元(银牛卡)</option>
						    <option value="888">888元(金牛卡）</option>
						</select>
					</li>		
				</ul>
			</div>
		</form>
		<div class="footer">
			<button class="but1" id="nextBtn">购买</button>
		</div>
	</body>
	<script>
		$(function(){
			$("#nextBtn").on('click',function(){
				var total_fee = $("#total_fee").val();
				if(total_fee.length == 0){
					cvphp.msg({
	    				content: '请选择金额'
	    			});
					return false;
				}	
				$("#form1").submit();
			});
		});
	</script>
</html>