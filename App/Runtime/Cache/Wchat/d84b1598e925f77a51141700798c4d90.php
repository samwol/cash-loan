<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/bankCss.css" rel="stylesheet">
		<title>手机运营商验证- 信息认证 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<style>
		.footer{
			text-align:center;
			padding:10px;
		}
		.zhezhao{
		    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.5);
    z-index: 12;
		}
		.sendyzm{
		    position: absolute;
    top: 40%;
    left: 50%;
    z-index: 13;
    width: 200px;
    min-width: 100px;
    background: #fff;
    padding: 20px;
    margin-left: -100px;
    border-radius: 10px;
display: flex;
    flex-direction: column;
    /* justify-content: flex-end; */
    align-items: center;	
		}
		.sendyzm input{
			margin-top:10px;
		}
		.sendyzm button{
			margin-top:10px;
		}
	</style>
	<body>
				<section class="ioc_list">
			<ul>
				<li class="col-xs-7 identityAuth"></li>
				<li class="col-xs-7 contactsAuth"></li>
				<li class="col-xs-7 bankAuth"></li>
				<li class="col-xs-7 addessAuth"></li>
				<li class="col-xs-7 mobileAuth"></li>
				<li class="col-xs-7 taobaoAuth"></li>
			</ul>
		</section>
		<?php $actionName = ACTION_NAME; ?>
		<script>
			<?php if(is_array($auth)): $i = 0; $__LIST__ = $auth;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(!empty($vo)): ?>var obj = $(".ioc_list li."+"<?php echo ($key); ?>"+"Auth");
					obj.removeClass("<?php echo ($key); ?>"+"Auth");
					obj.addClass("<?php echo ($key); ?>"+"AuthNow");
					obj.html("<span></span>");<?php endif; endforeach; endif; else: echo "" ;endif; ?>
			var actionName = "<?php echo ($actionName); ?>";
			var obj = $(".ioc_list li."+actionName);
			obj.removeClass(actionName);
			obj.addClass(actionName+"Now");
		</script>
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12">
						<label>手机号</label>
						<input type="text" placeholder="请输入手机号" class="form-control" name="mobile" />
					</li>				
					<li class="col-xs-12">
						<label>服务密码</label>
						<input type="text" placeholder="请输入服务密码" class="form-control" name="fwpass" />
					</li>

				</ul>
					<div style="font-size:14px;float:left;width:50%;color:black;margin-top:10px;" onclick="sendSms()">快速发送短信设置服务密码</div>
					<div style="font-size:14px;float:right;width:50%;text-align: right;color:black;margin-top:10px;" onclick="telPhone()">快速拨打客服电话查询</div>
			</div>
			<div class="yanzheng" style="display:none;">
			<div class="zhezhao"></div>
			<div class="sendyzm">
						<label>验证码</label>
						<input type="text" placeholder="请输入验证码" class="form-control" name="verifycode" />
						<button type="button" class="btn btn-primary" id="yzm">确认</button>
			</div>
			</div>
		<div class="footer">
			<button type="button" class="btn btn-primary" id="query">确认授权</button>
			<button type="button" class="btn btn-primary" id="next">跳过</button>
		</div>
	</body>
	<script>
		/*function resend(){
			if(resendTime == 1){
				$("#yzm").html('重新发送');
				clearInterval(resendFun);
				$("#yzm").removeAttr('disabled');
			}else{
				resendTime--;
				$("#yzm").html(resendTime + ' 秒重试');
				$("#yzm").attr('disabled',"true");
			}
		}*/
        function checkTelphone(telphone){
            var isChinaMobile = /^1(3[4-9]|5[012789]|8[23478]|4[7]|7[8])\d{8}$/; //移动方面最新答复
            var isChinaUnion = /^1(3[0-2]|5[56]|8[56]|4[5]|7[6])\d{8}$/; //向联通微博确认并未回复
            var isChinaTelcom = /^1(3[3])|(8[019])\d{8}$/; //1349号段 电信方面没给出答复，视作不存在
            var isOtherTelphone  = /^170([059])\\d{7}$/;//其他运营商
            if(telphone.length !== 11){
                return {status:false,msg:"未检测到正确手机号"}
            } else{
                if(isChinaMobile.test(telphone)){
                    // 移动
                    return {status:true,msg:'ChinaMobile'}
                }
                else if(isChinaUnion.test(telphone)){
                    // 联通
                    return {status:true,msg:'ChinaUnion'}
                }
                else if(isChinaTelcom.test(telphone)){
                    // 电信
                    return {status:true,msg:'ChinaTelcom'};
                }
                else if(isOtherTelphone.test(telphone)){
                    var num = isOtherTelphone.exec(telphone);
                    return {status:false,msg:"未检测到正确手机号"}
                }
                else{
                    return {status:false,msg:"未检测到正确手机号"}
                }
            }
        }
		function sendSms(){
            var mobile = $("input[name='mobile']").val();
            var obj = checkTelphone(mobile);
            if(!obj.status){
                alert(obj.msg);
                return;
			}
			switch(obj.msg){
				case 'ChinaMobile': // 移动
                    clickA("sms:10086");
				    break;
                case 'ChinaUnion': // 联通
                    clickA("sms:10010");
                    break;
                case 'ChinaTelcom': // 电信
                    clickA("sms:10000");
                    break;
			}
		}
		function telPhone(){
            var mobile = $("input[name='mobile']").val();
            var obj = checkTelphone(mobile);
            if(!obj.status){
                alert(obj.msg);
                return;
            }
            switch(obj.msg){
                case 'ChinaMobile': // 移动
                    clickA("tel:10086");
                    break;
                case 'ChinaUnion': // 联通
                    clickA("tel:10010");
                    break;
                case 'ChinaTelcom': // 电信
                    clickA("tel:10000");
                    break;
            }
		}

		function clickA(url)
		{
            var link= $('<a href="'+url+'"></a>');
            link.get(0).click();
		}

		var $token = '';
		var time = 1;
		var rw = null;
		function getstatus(token){
				rw = setInterval(function(){
				cvphp.mpost("<?php echo U('Info/status');?>",{token:$token},function(data){
					console.log(data.msg);
					if(data.code == '0006'){
						layer.closeAll();
						$(".yanzheng").show();
						clearInterval(rw);
					}else if(data.code == '0000'){
						cvphp.mpost(
							"<?php echo U('Info/mobileAuth');?>",
							{
								token:$token
							},
							function(data){
								if(data.status != 1){
									//$token = data.token;
									//getstatus($token);
									layer.closeAll();
									cvphp.msg({
										content: data.info
									});
									console.log(data);
								}else{
									cvphp.msg({
										content: "授权成功"
									});
									clearInterval(rw);
									//window.location.reload();
									location.href="<?php echo U('Info/taobaoAuth');?>";
								}
							}
						);	
						clearInterval(rw);						
					}else if(data.code == '1102'){
								cvphp.msg({
										content: "帐号密码错误"
								});
								layer.closeAll();
								clearInterval(rw);
					}
				});
				time ++;
				if(time >= 200){
					cvphp.msg({
						content: "授权超时"
					});				
					clearInterval(rw);
				}
			},3000);			
		}
		$(function(){
			$("#query").click(function(){
				var mobile = $("input[name='mobile']").val();
				var fwpass = $("input[name='fwpass']").val();
				//var verifycode = $("input[name='verifycode']").val();
				if(mobile.length == 0){
					cvphp.msg({
	    				content: '请输入手机号'
	    			});
					return false;
				}				
				if(fwpass.length == 0){
					cvphp.msg({
	    				content: '请输入服务密码'
	    			});
					return false;
				}
				if(!cvphp.ismobile(mobile)){
					cvphp.msg({
	    				content: '请输入规范的手机号'
	    			});
					return false;
				}
				  layer.open({
					type: 2
					,content: '读取中请勿退出'
				  });				
				cvphp.mpost(
					"<?php echo U('Info/mobile');?>",
					{
						fwpass:fwpass,
						mobile:mobile,
					},
					function(data){
					    console.log(data)
						if(data.status == 1){
                            setTimeout(function(){
                                alert("授权成功");
                                layer.closeAll();
                                location.href="<?php echo U('Info/taobaoAuth');?>";
							},5000)
						}else{
                            cvphp.msg({
                                content: "授权失败"
                            });
						}
						// if(data.code == "0010"){
						// 	console.log(data.msg);
						// 	$token = data.token;
						// 	getstatus($token);
						// }
					}
				);
			
			});	
			$("#yzm").on('click',function(){
				var verifycode = $("input[name='verifycode']").val();
				if(verifycode.length == 0){
					cvphp.msg({
	    				content: '请输入验证码'
	    			});
					return;
				}
				cvphp.mpost(
					"<?php echo U('Info/sendVerify');?>",
					{
						token:$token,
						verifycode:verifycode
					},
					function(data){
						console.log(data.msg);
						if(data.code == "0009"){
							$("input[name='verifycode']").val('');
							$(".yanzheng").hide();
							layer.open({
								type: 2
								,content: '读取中请勿退出'
							 });
							getstatus($token);
						}
					}
				);
			});
			$("#nextBtn").on('click',function(){
				var mobile = $("input[name='mobile']").val();
				var fwpass = $("input[name='fwpass']").val();
				var verifycode = $("input[name='verifycode']").val();
				if(mobile.length == 0){
					cvphp.msg({
	    				content: '请输入手机号'
	    			});
					return false;
				}				
				if(fwpass.length == 0){
					cvphp.msg({
	    				content: '请输入服务密码'
	    			});
					return false;
				}
				if(verifycode.length == 0){
					cvphp.msg({
	    				content: '请输入验证码'
	    			});
					return false;
				}				
				if(!cvphp.ismobile(mobile)){
					cvphp.msg({
	    				content: '请输入规范的手机号'
	    			});
					return false;
				}
				cvphp.submit($("form"),function(data){
					if(data.status != 1){
						cvphp.msg({
		    				content: data.info
		    			});
						return false;
					}else{
						cvphp.msg({
		    				content: '保存成功'
		    			});
		    			var url = data.url;
		    			if(url.length > 0){
		    				setTimeout(function(){
		    					window.location.href = url;
		    				},2000);
		    			}
					}
				});
			});
			$("#next").on('click',function(){
				cvphp.mpost(
					"<?php echo U('Info/next');?>",
					{
						name:'mobile',						
					},
					function(data){
							if(data.status != 1){
								cvphp.msg({
									content: data.info
								});
								console.log(data);
							}else{
								//window.location.reload();
								location.href="<?php echo U('Info/taobaoAuth');?>";
							}
					}
				);
				//location.href="<?php echo U('Info/taobaoAuth');?>";
			});			
		});
	</script>
</html>