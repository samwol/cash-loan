<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Wchat/css/dhkCss.css" />
		<title>支付方式 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>  - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
		<style>
			.paytype{
			    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
   min-height: 28rem;
    width: 100%;
	    background: url(/Public/Wchat/images/dongtu.gif) no-repeat #1776d4;
    background-size: 100%;
    background-position-y: -5.5rem;
			}
			.paytype .weixin{
			    width: 5rem;
    height: 5rem;
    background: #44b549;
    border-radius: 100%;
    line-height: 5rem;
    text-align: center;
    color: #fff;
    font-size: 18px;
    font-weight: bold;
    box-shadow: 0 0 5px #44b549;
	    margin: 3rem;
			}
			.paytype .zhifubao{
    width: 5rem;
    height: 5rem;
    background: #F36948;
    border-radius: 100%;
    line-height: 5rem;
    text-align: center;
    color: #fff;
    font-size: 18px;
    font-weight: bold;
    box-shadow: 0 0 5px #F36948;
    margin: 3rem;	
			}
			.logo {
	width: 100%;
	min-height: 2rem;
	float: left;
	text-align: center;
}

a:hover {
	text-decoration: none;
}

		</style>
	</head>
	<body>
	<div class="logo">
	    	<img src="/Public/Wchat/images/re_logo.png">
	    </div>
		<div class="paytype">
		<a class="weixin" href="<?php echo U('Pay/alipay',array('order' => $data,'paytype'=>3));?>">微信</a>
		<a class="zhifubao" href="<?php echo U('Pay/alipay',array('order' => $data,'paytype'=>1));?>">支付宝</a>
		</div>
		<script>
			//$('.paytype').css('height',window.innerHeight+"px");
		</script>
	</body>
</html>