<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<title>签署本次借款合同 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
		<script src="__PUBLIC__/Wchat/js/jSignature.min.js"></script>
        <!--[if lt IE 9]>
        <script src="__PUBLIC__/Wchat/js/flashcanvas.js"></script>
        <![endif]-->
		<style type="text/css">
			#signature {
				border: 2px dotted black;
				background-color:lightgrey;
			}
			.textTitle{
				text-align: center;
				margin-top: 2rem;
			}
			.btnDiv{
				width: 90%;
				margin: 0 auto;
			}
			.btnDiv a,a:hover{
				width: 48%;
			    display: inline-block;
			    text-align: center;
			    line-height: 3rem;
			    height: 3rem;
			    margin-top: 0.5rem;
			    color: #fff;
			    border-radius: 3%;
			    text-decoration:none;
			}
		</style>
	</head>
	<body>
		<div class="textTitle">请在虚线内签字,签名需规范、完整、字迹清晰且必须为本人签名.</div>
		<div id="signature" style="height: 18rem;width: 90%;margin: 0 auto;"></div>
		<form action="<?php echo U('Loan/signature');?>" method="post">
			<input type="hidden" name="signature" value="" />
		</form>
		<div class="btnDiv">
			<a href="javascript:;" id="resetBtn" style="background-color: rgba(243, 182, 13, 0.83);">复位</a>
			<a href="javascript:;" id="doneBtn" style="background-color: #3699ef; float: right;">提交</a>
		</div>
		<script>
			var $sigdiv = $("#signature").jSignature('init',{height:'18rem',width:'100%'});
			$(function(){
				$("#resetBtn").on('click',function(){
					$sigdiv.jSignature("reset");
				});
				$("#doneBtn").on('click',function(){
					var datapair = $sigdiv.jSignature("getData", "image");
					$("input[name='signature']").val(datapair[1]);
					cvphp.submit($("form"),function(data){
						if(data.status != 1){
							cvphp.msg({
		    					content: data.info
		    				});
							if(data.url != ""){
								setTimeout(function(){
									window.location.href = data.url;
								},2000);
							}
						}else{
							window.location.href = data.url;
						}
					});
				});
			});
		</script>
	</body>
</html>