<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Wchat/css/dhkCss.css" />
		<title>待还款订单 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>  - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>-汇码网（www.huimaw.com）</title>
	<style>
		.con_right .b{
		line-height: 3rem;
		font-size: 0.95rem;
		color: #0abfff;
		position: absolute;
		right: 9%;
		bottom: 0;
		background: url(../images/fh.png) center right no-repeat;
		background-size: 17px;
		padding-right: 10%;
		}
		.con_right .t{
		line-height: 3rem;
		font-size: 0.95rem;
		color: #0abfff;
		position: absolute;
		right: 9%;
		top: 0;
		background: url(../images/fh.png) center right no-repeat;
		background-size: 17px;
		padding-right: 10%;
		}
		.con_right .z{
		line-height: 5.8rem;
		font-size: 0.95rem;
		color: #0abfff;
		position: absolute;
		right: 9%;
		top: 0;
		background: url(../images/fh.png) center right no-repeat;
		background-size: 17px;
		padding-right: 10%;
		}			
	</style>
	</head>
	<body>
		<div class="hk_head">
			<p>待还款金额￥<?php echo ($noneMoney); ?>元</p>
		</div>
		<div class="hk_list">
<?php $empty="<span style='text-align: center;display: block;margin-top: 1rem;'>暂时没有订单</span>"; ?>
<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo['pending'] == 0): ?><div class="one_hk" onclick="window.location.href='<?php echo U('Publicproject/index');?>'">
				<div class="ON">
					<a href="javascript:;">借款订单号：<?php echo ($vo["oid"]); ?></a>
				</div>
				<div class="hk_con">
					<div class="con_left" style="background-color: #3ed050;">
						<h2>审核中</h2>
						<span>请等待</span>
					</div>
					<div class="con_right">
						<h2><?php echo (date("Y.m.d",$vo["add_time"])); ?></h2>
						<!--<span>等待确认签名后还款</span>-->
						<span>购买商城额度放款（购买）</span>
					</div>
				</div>
			</div>
	<?php elseif($vo['pending'] == 2): ?>
		<div class="one_hk">
			<div class="ON">
				<a href="javascript:;">借款订单号：<?php echo ($vo["oid"]); ?></a>
			</div>
			<div class="hk_con">
				<div class="con_left" style="background-color: #f54747;">
					<h2>未通过</h2>
					<span>申请驳回</span>
				</div>
				<div class="con_right">
					<h2><?php echo (date("Y.m.d",$vo["add_time"])); ?></h2>
					<span><?php echo ($vo["error"]); ?></span>
				</div>
			</div>
		</div>
	<?php else: ?>
		<div class="one_hk">
			<div class="ON">
				<a href="<?php echo U('Repay/viewbill',array('oid'=>$vo['id']));?>">借款订单号：<?php echo ($vo["oid"]); ?></a>
			</div>
			<div class="hk_con">
				<div class="con_left" onclick="window.location.href = '<?php echo U('Repay/viewbill',array('oid'=>$vo['id']));?>';">
					<h2>￥<?php echo ($vo["bill"]["allmoney"]); ?></h2>
					<span>第<?php echo ($vo["bill"]["billnum"]); ?> / <?php echo ($vo["bill"]["allbill"]); ?>期</span>
				</div>
				<div class="con_right">
					<h2><?php echo (date("Y.m.d",$vo["bill"]["repayment_time"])); ?></h2>
<?php if($vo['bill']['status'] == 0): ?><!--<span>距离还款日<?php echo ($vo["bill"]["timelenth"]); ?>天</span>-->
					<span>预计到账 4-6 小时</span>
					<a class="z" href="<?php echo U('Repay/repayment',array('id'=>$vo['bill']['id']));?>">还款</a><?php endif; ?>
<?php if($vo['bill']['status'] == 1): ?><span style="color: red;">已逾期<?php echo ($vo["bill"]["timelenth"]); ?>天</span>
					<a class="b" href="<?php echo U('Repay/repaymentXq',array('id'=>$vo['bill']['id']));?>">续期</a>
					<a class="t" href="<?php echo U('Repay/repayment',array('id'=>$vo['bill']['id']));?>">还款</a><?php endif; ?>
					
				</div>
			</div>
		</div><?php endif; endforeach; endif; else: echo "$empty" ;endif; ?>
		</div>
	</body>
</html>