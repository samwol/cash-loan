<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/MyCss.css" rel="stylesheet">
		<title>更多 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
		<div class="head_portrait">
			<div class="header">
				<img src="__PUBLIC__/Wchat/images/icon_weixian.png"><br/>
<?php $user = getUserInfo(); if ($user['vipid']==0){ $viptitle="普通卡"; }else if($user['vipid']==1){ $viptitle="银牛卡"; }else if($user['vipid']==2){ $viptitle="金牛卡"; }else{ $viptitle="普通卡"; } ?>
</php>
<?php if(empty($user)): ?><a href="<?php echo U('Index/login');?>">登录 / 注册</a>
<?php else: ?>
				<span><?php echo getViewPhone($user['telnum']);?>(<?php echo ($viptitle); ?>)</span><?php endif; ?>
			</div>
		</div>
		<div class="nav">
			<ul>
				<!--<li class="col-xs-12 buy">
					<a href="<?php echo U('Page/buy');?>">会员购买</a>
				</li>-->
				<li class="col-xs-12 guanyu">
					<a href="<?php echo U('Page/about');?>">关于我们</a>
				</li>
				<li class="col-xs-12 one">
					<a href="<?php echo U('Index/fenxiang');?>">告诉朋友</a>
				</li>
				<li class="col-xs-12 three">
					<a href="<?php echo U('Page/problem');?>">常见问题</a>
				</li>
				<li class="col-xs-12 kefu">
					<a href="http://kefu.cckefu.com/vclient/chat/?websiteid=136596">在线客服</a>
				</li>
			</ul>
		</div>
<?php if(!empty($user)): ?><div class="nav">
			<ul>
				<li class="col-xs-12 system">
					<a href="<?php echo U('Page/setting');?>">设置</a>
				</li>
			</ul>
		</div>
		<div class="anniu">
			<a href="javascript:;" onclick="logout();">退出登录</a>
		</div>
		<script>
			function logout(){
				layer.open({
					content: '您确定要退出系统吗？'
					,btn: ['确定','取消']
					,yes: function(index){
						window.location.href = "<?php echo U('Index/logout');?>";
					}
				});
			}
		</script><?php endif; ?>
		<link href="__PUBLIC__/Wchat/css/footer.css" rel="stylesheet">
<!--<div style="clear: both; height: 3.2rem;"></div>-->
<div class="foot">
	<ul>
    	<li class="col-xs-3 index">
        	<a href="<?php echo U('Index/index');?>">首页</a>
        </li>
        <li class="col-xs-3 withdraw">
        	<a href="<?php echo U('Repay/index');?>">钱包</a>
        </li>
        <li class="col-xs-3 public">
            <a href="<?php echo U('Publicproject/index');?>">增信商城</a>
        </li>
        <li class="col-xs-3 more_sel">
        	<a href="<?php echo U('Index/more');?>">更多</a>
        </li>
    </ul>
</div>
	</body>
</html>