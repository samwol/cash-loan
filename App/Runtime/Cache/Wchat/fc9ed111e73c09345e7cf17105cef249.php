<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/bankCss.css" rel="stylesheet">
		<title>绑定收款银行卡 - 信息认证 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?> - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
				<section class="ioc_list">
			<ul>
				<li class="col-xs-7 identityAuth"></li>
				<li class="col-xs-7 contactsAuth"></li>
				<li class="col-xs-7 bankAuth"></li>
				<li class="col-xs-7 addessAuth"></li>
				<li class="col-xs-7 mobileAuth"></li>
				<li class="col-xs-7 taobaoAuth"></li>
			</ul>
		</section>
		<?php $actionName = ACTION_NAME; ?>
		<script>
			<?php if(is_array($auth)): $i = 0; $__LIST__ = $auth;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(!empty($vo)): ?>var obj = $(".ioc_list li."+"<?php echo ($key); ?>"+"Auth");
					obj.removeClass("<?php echo ($key); ?>"+"Auth");
					obj.addClass("<?php echo ($key); ?>"+"AuthNow");
					obj.html("<span></span>");<?php endif; endforeach; endif; else: echo "" ;endif; ?>
			var actionName = "<?php echo ($actionName); ?>";
			var obj = $(".ioc_list li."+actionName);
			obj.removeClass(actionName);
			obj.addClass(actionName+"Now");
		</script>
		<form action="<?php echo U('Info/bankAuth');?>" method="post">
			<div class="row xinxi">
				<ul>
					<li class="col-xs-12">
						<label>开户行</label>
						<input type="text" placeholder="请输入开户行" class="form-control" name="bankName" />
					</li>				
					<li class="col-xs-12">
						<label>开户卡号</label>
						<input type="text" placeholder="请输入银行卡号" class="form-control" name="bankNum" />
					</li>
					<li class="col-xs-12">
						<label>银行预留手机号</label>
						<input type="text" placeholder="请输入预留手机号码" class="form-control" name="bankPhone" />
					</li>
				</ul>
			</div>
		</form>
		<div class="footer">
			<button class="but1" id="nextBtn">下一步</button>
		</div>
	</body>
	<script>
		$(function(){
			$("#nextBtn").on('click',function(){
				var bankNum = $("input[name='bankNum']").val();
				var bankPhone = $("input[name='bankPhone']").val();
				var bankName = $("input[name='bankName']").val();
				if(bankName.length == 0){
					cvphp.msg({
	    				content: '请输入开户行'
	    			});
					return false;
				}				
				if(bankNum.length == 0){
					cvphp.msg({
	    				content: '请输入银行卡号'
	    			});
					return false;
				}
				if(!cvphp.ismobile(bankPhone)){
					cvphp.msg({
	    				content: '请输入规范的手机号'
	    			});
					return false;
				}
				cvphp.submit($("form"),function(data){
					if(data.status != 1){
						cvphp.msg({
		    				content: data.info
		    			});
						return false;
					}else{
						cvphp.msg({
		    				content: '保存成功'
		    			});
		    			var url = data.url;
		    			if(url.length > 0){
		    				setTimeout(function(){
		    					window.location.href = url;
		    				},2000);
		    			}
					}
				});
			});
		});
	</script>
</html>