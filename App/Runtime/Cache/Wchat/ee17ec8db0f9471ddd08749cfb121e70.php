<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link href="__PUBLIC__/Wchat/css/bankCss.css" rel="stylesheet">
		<title>资料审核中 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>  - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
		<div class="banner"><img src="__PUBLIC__/Wchat/images/banner.png"></div>
		<div class="tishi">
			<img src="__PUBLIC__/Wchat/images/qy.png">
			<h2 class="shenhe">资料正在审核中</h2>
			<p>客官不要急，请耐心等待</p>
		</div>
		<div class="jindu">
			<h3>审批进度</h3>
			<div class="row future">
				<ul>
					<li class="col-xs-4 cg">
						<span></span>
						<div class="xian"></div>
						<em>提交资料</em>
					</li>
					<li class="col-xs-4 cg">
						<span></span>
						<div class="xian"></div>
						<em>审核中</em>
					</li>
					<li class="col-xs-4 tongguo">
						<span></span>
						<div class="xian"></div>
						<em>审核通过</em>
					</li>
					<li class="col-xs-4 tongguo">
						<span></span>
						<em>获得额度</em>
					</li>
				</ul>
			</div>
		</div>
		<link href="__PUBLIC__/Wchat/css/footer.css" rel="stylesheet">
<!--<div style="clear: both; height: 3.2rem;"></div>-->
<div class="foot">
	<ul>
    	<li class="col-xs-3 index_sel">
        	<a href="<?php echo U('Index/index');?>">首页</a>
        </li>
        <li class="col-xs-3 withdraw">
        	<a href="<?php echo U('Repay/index');?>">钱包</a>
        </li>
        <li class="col-xs-3 public">
            <a href="<?php echo U('Publicproject/index');?>">增信商城</a>
        </li>
        <li class="col-xs-3 more">
        	<a href="<?php echo U('Index/more');?>">更多</a>
        </li>
    </ul>
</div>
	</body>
</html>