<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Wchat/css/style.css">
		<script src="__PUBLIC__/Wchat/js/jquery.range.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<title><?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>  - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
<?php $MoneyScale = getMoneyScale(); $Interest = getInterest(); $Deadline = getDeadlineList(); $DeadlineStr = $Deadline['str']; $DeadlineList = $Deadline['list']; $user = getUserInfo(); ?>
		<div class="banner">
			<img src="__PUBLIC__/Wchat/images/banner.jpg">
		</div>
	    <!--借款金额-->
	    <div class="jkje">
	        <div class="title">
	        	<hr>
	            <span>借款金额</span>
	        </div>
	        <div class="siwt">
	        	<a href="javascript:;" class="jian">
	        		<img src="__PUBLIC__/Wchat/images/jian.png">
	        	</a>
	        	<a href="javascript:;" class="jia">
	        		<img src="__PUBLIC__/Wchat/images/jia.png">
	        	</a>
	        	<span class="cenjine"><?php echo ($MoneyScale["min"]); ?></span>
	        	<input type="hidden" class="single-slider" value="0" />
	        </div>
	        <div class="shuzhi">
	        	<ul>
	            	<li><?php echo ($MoneyScale["min"]); ?>元</li>
	                <li><?php echo ($MoneyScale["max"]); ?>元</li>
	           </ul>
	        </div>
	    </div>
	    <!--借款金额-->
	    <!--借款期限-->
	    <div class="jkqx">
	        <div class="title">
	        	<hr>
	            <span>借款期限</span>
	        </div>
	        <div class="qixian">
	        	<ul>
<?php if(is_array($DeadlineList)): $i = 0; $__LIST__ = array_slice($DeadlineList,0,1,true);if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li class="action" data="<?php echo ($vo); ?>"><?php echo ($vo); echo ($DeadlineStr); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
<?php if(is_array($DeadlineList)): $i = 0; $__LIST__ = array_slice($DeadlineList,1,null,true);if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li data="<?php echo ($vo); ?>"><?php echo ($vo); echo ($DeadlineStr); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
	            </ul>
	        </div>
	        <div class="daoqi">
	        	<ul>
	            	<li class="AllMoney">
	                	<span>应还</span>
	                    <strong></strong>
	                </li>
	                <li class="AllInterest">
	                	<span>服务费</span>
	                    <strong></strong>
	                </li>
	                <li class="LoanTime">
	                	<span>借贷期限</span>
	                    <strong></strong>
	                </li>
	           </ul>
	        </div>
	        <div class="tedian">
	        	<ul>
	            	<li>
	            		<span>秒审核</span>
	            	</li>
	                <li>
	                	<span>费率低</span>
	                </li>
	                <li>
	                	<span>下款快</span>
	                </li>
	            </ul>
	        </div>
	    </div>
	    <!--借款期限-->
	    <div class="con">
<?php if(empty($user)): ?><a href="<?php echo U('Index/login');?>" class="but1">申请借款</a>
<?php else: ?>
			<a href="javascript:void(0)" class="but1" id="shenqing">申请借款</a><?php endif; ?>
	        <p><img src="__PUBLIC__/Wchat/images/queren.png">我以阅读并同意<a href="<?php echo U('Page/protocol');?>">《<?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>服务协议》</a></p>
	    </div>
		<link href="__PUBLIC__/Wchat/css/footer.css" rel="stylesheet">
<!--<div style="clear: both; height: 3.2rem;"></div>-->
<div class="foot">
	<ul>
    	<li class="col-xs-3 index_sel">
        	<a href="<?php echo U('Index/index');?>">首页</a>
        </li>
        <li class="col-xs-3 withdraw">
        	<a href="<?php echo U('Repay/index');?>">钱包</a>
        </li>
        <li class="col-xs-3 public">
            <a href="<?php echo U('Publicproject/index');?>">增信商城</a>
        </li>
        <li class="col-xs-3 more">
        	<a href="<?php echo U('Index/more');?>">更多</a>
        </li>
    </ul>
</div>
	    <div class="alert1">
	    	<div class="win1">
	            <p>确定借款信息<a href="javascript:void(0)" id="gaun">关闭</a></p>
	            <div class="xinxi">
	                <ul>
	                    <li class="col-xs-12" to="money">
	                        <label>借款金额</label>
	                        <span>0元</span>
	                    </li>
						<li class="col-xs-12" to="truemoney">
							<label>实际到账</label>
							<span>0元</span>
						</li>
	                    <li class="col-xs-12" to="productlines">
	                        <label>商城额度</label>
	                        <span>0元</span>
	                    </li>
	                    <li class="col-xs-12" to="bank">
	                        <label>收款账户</label>
	                        <span></span>
	                    </li>
	                    <li class="col-xs-12" to="interest">
	                        <label>费率</label>
	                        <span>0%</span>
	                    </li>
	                    <li class="col-xs-12" to="loantime">
	                        <label>起始日期</label>
	                        <span></span>
	                    </li>
	                </ul>
	            </div>
	            <div class="xinxi xinxi1">
	                <ul>
	                    <li class="col-xs-12" to="fastrepayment">
	                        <label>首次还款日</label>
	                        <span>01/04</span>
	                    </li>
	                    <li class="col-xs-12" to="repaymenttime">
	                        <label>还款日</label>
	                        <span>每月4日</span>
	                    </li>
	                    <li class="col-xs-12" to="time">
	                        <label>借款期限</label>
	                        <span>5个月（期）</span>
	                    </li>
	                    <li class="col-xs-12 Agreement">我以阅读并同意
	                    	<a href="<?php echo U('Loan/viewContract');?>">《借款和担保协议》</a>
	                    </li>
	                </ul>
	            </div>
	            <a href="<?php echo U('Loan/signature');?>" class="liji">签署借款协议</a>
	    	</div>
	    </div>
	</body>
	<script>
		$(function(){
			var Num_1 = (<?php echo ($MoneyScale["max"]); ?> - <?php echo ($MoneyScale["min"]); ?>) / 100;
			viewLoanInfo();
			$('.single-slider').jRange({
				from: parseInt(<?php echo ($MoneyScale["min"]); ?>),//滑动范围的最小值，数字，如0
				to: parseInt(<?php echo ($MoneyScale["max"]); ?>),//滑动范围的最大值，数字，如100
				step: parseInt(<?php echo ($MoneyScale["step"]); ?>),//步长值，每次滑动大小
				scale: [0*Num_1 + <?php echo ($MoneyScale["min"]); ?>, 25*Num_1 + <?php echo ($MoneyScale["min"]); ?>, 50*Num_1 + <?php echo ($MoneyScale["min"]); ?>, 75*Num_1 + <?php echo ($MoneyScale["min"]); ?>, 100*Num_1 + <?php echo ($MoneyScale["min"]); ?>],//滑动条下方的尺度标签，数组类型，如[0,50,100]
				format: '%s',//数值格式
				width: 100+"%",//滑动条宽度签
				onstatechange: function(){
					var Money = $(".single-slider").val();
					$(".siwt .cenjine").html(Money);
					viewLoanInfo();
				}
			});

			$(".qixian ul li").on('click',function(){
				var obj = $(this);
				$(".qixian ul").find(".action").removeClass('action');
				obj.addClass('action');
				viewLoanInfo();
			});

			//金额减按钮
			$(".siwt .jian").on('click',function(){
				var Money = $(".single-slider").val();
				if(Money >= (<?php echo ($MoneyScale["min"]); ?> + <?php echo ($MoneyScale["step"]); ?>)){
					Money = parseInt(Money) - <?php echo ($MoneyScale["step"]); ?>;
				}else if(Money > <?php echo ($MoneyScale["min"]); ?> && Money <= <?php echo ($MoneyScale["min"]); ?> + <?php echo ($MoneyScale["step"]); ?>){
					Money = parseInt(<?php echo ($MoneyScale["min"]); ?>);
				}
				$(".single-slider").val(Money);
				$('.single-slider').jRange('setValue', Money);
				$(".siwt .cenjine").html(Money);
				viewLoanInfo();
			});

			//金额加按钮
			$(".siwt .jia").on('click',function(){
				var Money = $(".single-slider").val();
				if(Money == 0){
					Money = <?php echo ($MoneyScale["min"]); ?> + <?php echo ($MoneyScale["step"]); ?>;
				}else{
					if(Money < <?php echo ($MoneyScale["max"]); ?> - <?php echo ($MoneyScale["step"]); ?>){
						Money = parseInt(Money) + <?php echo ($MoneyScale["step"]); ?>;
					}else if(Money < <?php echo ($MoneyScale["max"]); ?> && Money >= <?php echo ($MoneyScale["max"]); ?> - <?php echo ($MoneyScale["step"]); ?>){
						Money = parseInt(<?php echo ($MoneyScale["max"]); ?>);
					}
				}
				$(".single-slider").val(Money);
				$('.single-slider').jRange('setValue', Money);
				$(".siwt .cenjine").html(Money);
				viewLoanInfo();
			});

		});
		document.documentElement.ontouchstart = function(){
		    return true;
		}

		function viewLoanInfo(){
			var Money = $(".single-slider").val();
			if(Money == 0) Money = <?php echo ($MoneyScale["min"]); ?>;
			Money = cvphp.getmoney(Money);
			var Time  = $(".qixian ul").find(".action").attr('data');
			var Interest = <?php echo ($Interest); ?>;
			//利息 = 本金 * 利息 * 期限
			Interest = parseFloat(Interest);
			var AllInterest = cvphp.getmoney(Money * Interest * Time);
			AllInterest = cvphp.getmoney(AllInterest);
			//var AllMoney = cvphp.getmoney(parseFloat(Money) + parseFloat(AllInterest));
			var AllMoney = cvphp.getmoney(parseFloat(Money));
			//显示
			$(".daoqi .AllMoney strong").html("￥" + AllMoney);
			$(".daoqi .AllInterest strong").html("￥" + AllInterest);
			$(".daoqi .LoanTime strong").html(Time + '<?php echo ($DeadlineStr); ?>');
		}

		$("#shenqing").click(function(){
			var Money = $(".single-slider").val();
			if(Money == 0) Money = <?php echo ($MoneyScale["min"]); ?>;
			var Time  = $(".qixian ul").find(".action").attr('data');
			cvphp.post(
				"<?php echo U('Loan/getConfirmInfo');?>",
				{
					money: Money,
					time: Time
				},
				function(data){
					if(data.status != 1){
						cvphp.msg({
	    					content: data.info
	    				});
						if(data.url != ""){
							setTimeout(function(){
								window.location.href = data.url;
							},2000);
						}
					}else{
						var data = data.info;
						console.dir(data)
						var AllInterest = cvphp.getmoney(data.money * data.interest * data.time);
						AllInterest = cvphp.getmoney(AllInterest);
						var truemoney = data.money - AllInterest;
						$(".alert1 .xinxi li[to='money'] span").html(data.money + "元");
						$(".alert1 .xinxi li[to='truemoney'] span").html(data.money + "元");
						// $(".alert1 .xinxi li[to='truemoney'] span").html(truemoney + "元");
						// 商城额度
						$(".alert1 .xinxi li[to='productlines'] span").html((data.money - truemoney) + "元");
						$(".alert1 .xinxi li[to='bank'] span").html(data.bankname+'（'+data.banknum+'）');
						$(".alert1 .xinxi li[to='loantime'] span").html(data.starttime_str+'-'+data.endtime_str);
						$(".alert1 .xinxi li[to='interest'] span").html(data.interest+'%');
						$(".Agreement a").attr('href',data.contract);
						if(data.loantype == 1){
							$(".alert1 .xinxi li[to='interest'] label").html('月费率');
							$(".alert1 .xinxi li[to='fastrepayment'] span").html(data.fastrepayment_str);
							$(".alert1 .xinxi li[to='repaymenttime'] span").html('每月'+data.repaymenttime+'日');
							$(".alert1 .xinxi li[to='time'] span").html(data.time+'个月（期）');
						}else{
							$(".alert1 .xinxi li[to='interest'] label").html('日费率');
							$(".alert1 .xinxi li[to='fastrepayment']").remove();
							$(".alert1 .xinxi li[to='repaymenttime'] span").html(data.fastrepayment_str);
							$(".alert1 .xinxi li[to='time'] span").html(data.time+'天');
						}
						$(".alert1").show();
						$(".win1").animate({height:'toggle'});
					}
				}
			);

			//收集签名
			$("#ConfirmLoan").on('click',function(){
				$(".alert2").show();
			});



		});
		$("#gaun").click(function(){
			$(".win1").animate({height:'toggle'});
			setTimeout('$(".alert1").hide()',500);
		});
	</script>
</html>