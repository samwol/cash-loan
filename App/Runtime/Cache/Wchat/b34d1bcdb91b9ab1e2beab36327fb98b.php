<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
	<head>
				<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="keywords" content="<?php
 $value = C("siteKeywords"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<meta name="description" content="<?php
 $value = C("siteDescription"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>">
		<link href="__PUBLIC__/Wchat/css/bootstrap.css" rel="stylesheet">
		<script src="__PUBLIC__/Wchat/js/jquery.min.js"></script>
		<script src="__PUBLIC__/Wchat/js/jquery.form.js"></script>
		<script src="__PUBLIC__/Wchat/js/cvphp.js"></script>
		<script src="__PUBLIC__/Wchat/js/index.js"></script>
		<script src="__PUBLIC__/Wchat/layer_mobile/layer.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Wchat/css/QuotaCss.css">
		<title>我要还款 - <?php
 $value = C("siteName"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?>  - <?php
 $value = C("siteTitle"); $content = ''; if($value){ $content = htmlspecialchars_decode(htmlspecialchars_decode($value)); } echo $content; ?></title>
	</head>
	<body>
		<div class="banner"><img src="__PUBLIC__/Wchat/images/banner.png"></div>
		<div class="ed">
			<div class="edu">
				<span>可用额度</span>
				<h3>￥<?php echo ($doquota); ?></h3>
				<span>总额度：￥<?php echo ($quota); ?></span>
				<a href="<?php echo U('Index/index');?>">快速借出</a>
			</div>
		</div>
		<div class="content">
			<ul>
				<li class="col-xs-12">
					<label>本期待还款</label>
					<span>￥<?php echo ($money); ?></span>
				</li>
				<li class="col-xs-12">
					<label>还款时间</label>
					<span><?php echo (date("Y/m/d",$time)); ?></span>
				</li>
			</ul>
			<div class="but">
				<!--<a href="<?php echo U('Repay/repayment',array('id'=>$billId));?>" class="but1">当期还款</a>-->
				<a href="#" onclick="notpay()" class="but1">当期还款</a>
				<a href="<?php echo U('Repay/order');?>" class="but2">查看更多</a>
			</div>
		</div>
		<link href="__PUBLIC__/Wchat/css/footer.css" rel="stylesheet">
<div style="clear: both; height: 3.2rem;"></div>
<div class="foot">
    <ul>
        <li class="col-xs-3 index">
            <a href="<?php echo U('Index/index');?>">首页</a>
        </li>
        <li class="col-xs-3 withdraw_sel">
            <a href="<?php echo U('Repay/index');?>">钱包</a>
        </li>
        <li class="col-xs-3 public">
            <a href="<?php echo U('Publicproject/index');?>">增信商城</a>
        </li>
        <li class="col-xs-3 more">
            <a href="<?php echo U('Index/more');?>">更多</a>
        </li>
    </ul>
</div>
	</body>
	<script>
		function notpay()
		{
		    alert("您当前无还款信息");
		    window.location.href = "<?php echo U('Index/index');?>";
		}
	</script>
</html>