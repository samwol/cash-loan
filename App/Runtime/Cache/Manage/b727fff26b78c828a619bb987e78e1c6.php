<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
<link rel="stylesheet" href="__PUBLIC__/Manage/css/bootstrap.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/web-icons/web-icons.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/font-awesome/font-awesome.css">
<script src="__PUBLIC__/Manage/js/jquery.js"></script>
<script src="__PUBLIC__/Manage/js/jquery.form.js"></script>
<script src="__PUBLIC__/Manage/js/bootstrap.js"></script>
<script src="__PUBLIC__/Manage/js/layer/layer.js"></script>
<script src="__PUBLIC__/Manage/js/cvphp.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Manage/css/table.css">
		<title>已还清订单</title>
	</head>
	<body>
		<div class="nestable">
			<div class="console-title console-title-border drds-detail-title clearfix">
				<h5>回款借款</h5>
			</div>
			<form method="get" id="seachForm">
				<input type="hidden" name="m" value="Loan" />
				<input type="hidden" name="a" value="payoff" />
				<div class="public-selectArea">
					<div class="clearfix">
						<div class="wp_box col-xs-6">
							<dl>
								<dt>订单号：</dt>
								<dd>
									<input type="text" name="s-oid" value="<?php echo ($_GET['s-oid']); ?>">
								</dd>
							</dl>
						</div>
						<div class="wp_box col-xs-6">
							<dl>
								<dt>申请时间：</dt>
								<dd>
									<input type="date" class="time-inp" name="s-timeStart" value="<?php echo ($_GET['s-timeStart']); ?>" />
								</dd>
								<dd>
									<input type="date" class="time-inp" name="s-timeEnd" value="<?php echo ($_GET['s-timeEnd']); ?>" />
								</dd>
							</dl>
						</div>
					</div>
					<div class="btnArea">
						<a href='javascript:$("#seachForm").submit();' class="btn btn-sereachBg">
							<i class="glyphicon glyphicon-search public-ico"></i>
							<span class="public-label">查询</span>
						</a>
					</div>
				</div>
			</form>
			<div class="scroll-bar-table">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>订单号</th>
							<th>用户名</th>
							<th>借款金额</th>
							<th>借款期限</th>
							<th>预期收益</th>
							<th>申请时间</th>
							<th>逾期次数</th>
							<th>合同</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr id="list-<?php echo ($vo["id"]); ?>">
							<td><?php echo ($vo["oid"]); ?></td>
							<td><?php echo ($vo["user"]["telnum"]); ?></td>
							<td><?php echo ($vo["money"]); ?>元</td>
							<td>
								<?php echo ($vo["time"]); if($vo['timetype'] == 1): ?>个月<?php else: ?>天<?php endif; ?>
							</td>
							<td><?php echo ($vo["interest_money"]); ?>元</td>
							<td><?php echo (date("Y/m/d H:i:s",$vo["add_time"])); ?></td>
							<td><?php echo ($vo["BillNum"]); ?> 期共逾期 <?php echo ($vo["overdueBillNum"]); ?> 次</td>
							<td>
								<a href="<?php echo U('Loan/viewContract',array('id'=>$vo['id']));?>" title="点击查看合同" target="_blank">查看合同</a>
							</td>
							<td class="text-left">
								<a href="">查看账单</a>
							</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					</tbody>
				</table>
			</div>
			<div class="table-pagin-container">
				<div class="pull-right page-box">
					<?php echo ($page); ?>
				</div>
			</div>
		</div>
	</body>
	<script>
	</script>
</html>