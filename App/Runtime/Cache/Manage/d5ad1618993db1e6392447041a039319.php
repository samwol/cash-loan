<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
<link rel="stylesheet" href="__PUBLIC__/Manage/css/bootstrap.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/web-icons/web-icons.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/font-awesome/font-awesome.css">
<script src="__PUBLIC__/Manage/js/jquery.js"></script>
<script src="__PUBLIC__/Manage/js/jquery.form.js"></script>
<script src="__PUBLIC__/Manage/js/bootstrap.js"></script>
<script src="__PUBLIC__/Manage/js/layer/layer.js"></script>
<script src="__PUBLIC__/Manage/js/cvphp.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Manage/css/table.css">
		<title>待审核列表</title>
	</head>
	<body>
		<div class="nestable">
			<div class="console-title console-title-border drds-detail-title clearfix">
				<h5>借款审核</h5>
			</div>
			<form method="get" id="seachForm">
				<input type="hidden" name="m" value="Loan" />
				<input type="hidden" name="a" value="pending" />
				<div class="public-selectArea">
					<div class="clearfix">
						<div class="wp_box col-xs-6">
							<dl>
								<dt>订单号：</dt>
								<dd>
									<input type="text" name="s-oid" value="<?php echo ($_GET['s-oid']); ?>">
								</dd>
							</dl>
						</div>
						<div class="wp_box col-xs-6">
							<dl>
								<dt>申请时间：</dt>
								<dd>
									<input type="date" class="time-inp" name="s-timeStart" value="<?php echo ($_GET['s-timeStart']); ?>" />
								</dd>
								<dd>
									<input type="date" class="time-inp" name="s-timeEnd" value="<?php echo ($_GET['s-timeEnd']); ?>" />
								</dd>
							</dl>
						</div>
					</div>
					<div class="btnArea">
						<a href='javascript:$("#seachForm").submit();' class="btn btn-sereachBg">
							<i class="glyphicon glyphicon-search public-ico"></i>
							<span class="public-label">查询</span>
						</a>
					</div>
				</div>
			</form>
			<div class="scroll-bar-table">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>订单号</th>
							<th>用户名</th>
							<th>借款金额</th>
							<th>借款期限</th>
							<th>预期收益</th>
							<th>申请时间</th>
							<th>收款银行</th>
							<th>银行卡号</th>
							<th>开户名称</th>
							<th>合同</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr id="list-<?php echo ($vo["id"]); ?>">
							<td><?php echo ($vo["oid"]); ?></td>
							<td><?php echo ($vo["user"]["telnum"]); ?></td>
							<td><?php echo ($vo["money"]); ?>元</td>
							<td>
								<?php echo ($vo["time"]); if($vo['timetype'] == 1): ?>个月<?php else: ?>天<?php endif; ?>
							</td>
							<td><?php echo ($vo["interest_money"]); ?>元</td>
							<td><?php echo (date("Y/m/d H:i:s",$vo["add_time"])); ?></td>
							<td><?php echo ($vo["bankname"]); ?></td>
							<td><?php echo ($vo["banknum"]); ?></td>
							<td><?php echo ($vo["name"]); ?></td>
							<td>
								<a href="<?php echo U('Loan/viewContract',array('id'=>$vo['id']));?>" title="点击查看合同" target="_blank">查看合同</a>
							</td>
							<td class="text-left">
								<a href="javascript:setPendingStatus('<?php echo ($vo["id"]); ?>',1);">确认打款并生成账单</a>
								<a href="javascript:setPendingStatus('<?php echo ($vo["id"]); ?>',2);">驳回申请</a>
							</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					</tbody>
				</table>
			</div>
			<div class="table-pagin-container">
				<div class="pull-right page-box">
					<?php echo ($page); ?>
				</div>
			</div>
		</div>
	</body>
	<script>
		function setPendingStatus(id,status){
			var title;
			if(status == 1){
				title = '请先检验合同内容及签名并确认打款已成功再确认';
			}else if(status == 2){
				title = '驳回申请需剪短描述驳回原因';
			}else{
				return ;
			}
			layer.confirm(
				title,
				{
					btn: ['下一步','取消']
				},function(){
					layer.closeAll();
					var errorStr;
					if(status == 2){
						layer.prompt(
							{
								title:"请输入驳回原因",
								formType:0
							},
							function(str,index){
								errorStr = str;
								layer.close(index);
								cvphp.post(
									"<?php echo U('Loan/setPendingStatus');?>",
									{
										status:status,
										error:errorStr,
										id:id
									},
									function(data){
										if(data.status!=1){
											layer.msg(data.info);
										}else{
											$("#list-"+id).remove();
											layer.msg("操作成功");
										}
									}
								);
							}
						);
					}else{
						cvphp.post(
							"<?php echo U('Loan/setPendingStatus');?>",
							{
								status:status,
								id:id
							},
							function(data){
								if(data.status!=1){
									layer.msg(data.info);
								}else{
									$("#list-"+id).remove();
									layer.msg("操作成功");
								}
							}
						);
					}
				}
			);
		}
	</script>
</html>