<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
<link rel="stylesheet" href="__PUBLIC__/Manage/css/bootstrap.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/web-icons/web-icons.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/font-awesome/font-awesome.css">
<script src="__PUBLIC__/Manage/js/jquery.js"></script>
<script src="__PUBLIC__/Manage/js/jquery.form.js"></script>
<script src="__PUBLIC__/Manage/js/bootstrap.js"></script>
<script src="__PUBLIC__/Manage/js/layer/layer.js"></script>
<script src="__PUBLIC__/Manage/js/cvphp.js"></script>
    <link rel="stylesheet" href="__PUBLIC__/Manage/css/table.css">
    <script src="__PUBLIC__/Manage/js/wangEditor/wangEditor.min.js"></script>
    <title>新增与编辑商品</title>
</head>
<body>
<div class="nestable">
    <div class="console-title console-title-border drds-detail-title clearfix">
        <h5>新增与编辑商品</h5>
    </div>
    <div class="public-selectArea public-selectArea1">
        <form action="<?php echo U('Product/save');?>" method="post">
            <input type="hidden" value="<?php echo ($id); ?>" name="id">
            <div class="clearfix">
                <div class="wp_box  col-xs-8">
                    <dl>
                        <dt>产品名称：</dt>
                        <dd>
                            <input type="text" name="name" value="<?php echo ($product["name"]); ?>">
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="clearfix">
                <div class="wp_box  col-xs-8">
                    <dl>
                        <dt>简介：</dt>
                        <dd>
                            <textarea name="content" value="<?php echo ($product["content"]); ?>" id="" cols="30" rows="10"></textarea>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="clearfix">
                <div class="wp_box  col-xs-8">
                    <dl>
                        <dt>Wechat地址：</dt>
                        <dd>
                            <input type="text" name="wechat_url" value="<?php echo ($product["wechat_url"]); ?>">
                        </dd>
                    </dl>
                </div>
            </div>

            <div class="clearfix">
                <div class="wp_box  col-xs-8">
                    <dl>
                        <dt>Alipay地址：</dt>
                        <dd>
                            <input type="text" name="alipay_url" value="<?php echo ($product["alipay_url"]); ?>">
                        </dd>
                    </dl>
                </div>
            </div>

            <div class="clearfix">
                <div class="wp_box  col-xs-8">
                    <dl>
                        <dt>价格：</dt>
                        <dd>
                            <input type="number" name="price" value="<?php echo ($product["price"]); ?>">
                        </dd>
                    </dl>
                </div>
            </div>

            <div class="btnArea">
                <a href="javascript:;" class="btn btn-sereachBg" id="submitBtn">
                    <span class="public-label">提交</span>
                </a>
            </div>
        </form>
    </div>
</div>
</body>
<script>
    $(function(){
        $("#submitBtn").on('click',function(){
            cvphp.submit($("form"),function(data){
                console.log(data)
                if(data.status!=1){
                    layer.msg(data.info);
                }else{
                    layer.msg('编辑成功');
                    setTimeout(function(){
                        window.location.href="<?php echo U('Product/index');?>";
                    },2000);
                }
            });
        });
    });
</script>
</html>