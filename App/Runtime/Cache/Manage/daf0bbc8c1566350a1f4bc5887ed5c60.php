<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
<link rel="stylesheet" href="__PUBLIC__/Manage/css/bootstrap.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/web-icons/web-icons.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/font-awesome/font-awesome.css">
<script src="__PUBLIC__/Manage/js/jquery.js"></script>
<script src="__PUBLIC__/Manage/js/jquery.form.js"></script>
<script src="__PUBLIC__/Manage/js/bootstrap.js"></script>
<script src="__PUBLIC__/Manage/js/layer/layer.js"></script>
<script src="__PUBLIC__/Manage/js/cvphp.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Manage/css/table.css">
		<title>支付订单列表</title>
	</head>
	<body>
		<div class="nestable">
			<div class="console-title console-title-border drds-detail-title clearfix">
				<h5>支付订单</h5>
			</div>
			<form method="get" id="seachForm">
				<input type="hidden" name="m" value="Pay" />
				<input type="hidden" name="a" value="index" />
				<div class="public-selectArea">
					<div class="clearfix">
						<div class="wp_box col-xs-6">
							<dl>
								<dt>订单号：</dt>
								<dd>
									<input type="text" name="s-oid" value="<?php echo ($_GET['s-oid']); ?>">
								</dd>
							</dl>
						</div>
						<div class="wp_box col-xs-6">
		                	<dl>
		                    	<dt>状态：</dt>
		                        <dd>
		                          	<select class="select" name="status">
		                                <option value="" selected="selected">全部</option>
		                                <option value="0">未支付</option>
		                                <option value="1">已支付</option>
		                            </select>
		                        </dd>
		                    </dl>
						</div>
					</div>
					<div class="btnArea">
						<a href='javascript:$("#seachForm").submit();' class="btn btn-sereachBg">
							<i class="glyphicon glyphicon-search public-ico"></i>
							<span class="public-label">查询</span>
						</a>
					</div>
				</div>
			</form>
			<div class="scroll-bar-table">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>订单流水号</th>
							<th>用户名</th>
							<th>支付金额</th>
							<th>创建时间</th>
							<th>状态</th>
							<th>支付时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr id="list-<?php echo ($vo["id"]); ?>">
							<td><?php echo ($vo["id"]); ?></td>
							<td><?php echo ($vo["user"]["telnum"]); ?></td>
							<td><?php echo ($vo["money"]); ?>元</td>
							<td><?php echo (date("Y/m/d H:i:s",$vo["add_time"])); ?></td>
							<td>
								<?php if($vo['status'] == 1): ?>已支付<?php else: ?>未支付<?php endif; ?>
							</td>
							<td>
								<?php if($vo['status'] == 1): echo (date("Y/m/d H:i:s",$vo["pay_time"])); else: ?> -<?php endif; ?>
							</td>
							<td class="text-left">
								<a href="javascript:delPayOrder('<?php echo ($vo["id"]); ?>');">删除订单</a>
							</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					</tbody>
				</table>
			</div>
			<div class="table-pagin-container">
				<div class="pull-right page-box">
					<?php echo ($page); ?>
				</div>
			</div>
		</div>
	</body>
	<script>
		function delPayOrder(id){
			layer.confirm(
				'建议保留订单,订单删除后不可恢复,请确认？',
				{
					btn: ['确认删除','取消']
				},function(){
					cvphp.post(
						"<?php echo U('Pay/delOrder');?>",
						{
							id:id
						},
						function(data){
							if(data.status!=1){
								layer.msg(data.info);
							}else{
								$("#list-"+id).remove();
								layer.msg("操作成功");
							}
						}
					);
				}
			);
		}
	</script>
</html>