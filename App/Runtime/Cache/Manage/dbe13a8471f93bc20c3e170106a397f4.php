<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
<link rel="stylesheet" href="__PUBLIC__/Manage/css/bootstrap.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/web-icons/web-icons.css">
<link rel="stylesheet" href="__PUBLIC__/Manage/fonts/font-awesome/font-awesome.css">
<script src="__PUBLIC__/Manage/js/jquery.js"></script>
<script src="__PUBLIC__/Manage/js/jquery.form.js"></script>
<script src="__PUBLIC__/Manage/js/bootstrap.js"></script>
<script src="__PUBLIC__/Manage/js/layer/layer.js"></script>
<script src="__PUBLIC__/Manage/js/cvphp.js"></script>
		<link rel="stylesheet" href="__PUBLIC__/Manage/css/table.css">
		<script src="__PUBLIC__/Manage/js/wangEditor/wangEditor.min.js"></script>
		<title>编辑管理员</title>
	</head>
	<body>
		<div class="nestable">
			<div class="console-title console-title-border drds-detail-title clearfix">
				<h5>编辑管理员</h5>
			</div>
			<div class="public-selectArea public-selectArea1">
				<form action="<?php echo U('Admin/edit',array('id'=>$data['id']));?>" method="post">
					<div class="clearfix">
						<div class="wp_box  col-xs-8">
							<dl>
								<dt>用户名：</dt>
								<dd>
									<input type="text" name="username" value="<?php echo ($data["username"]); ?>">
								</dd>
							</dl>
						</div>
					</div>
					<div class="clearfix">
						<div class="wp_box  col-xs-8">
							<dl>
								<dt>密码：</dt>
								<dd>
									<input type="password" name="password" placeholder="如不修改请留空">
								</dd>
							</dl>
						</div>
					</div>
					<div class="clearfix">
						<div class="wp_box  col-xs-8">
							<dl>
								<dt>确认密码：</dt>
								<dd>
									<input type="password" name="repassword">
								</dd>
							</dl>
						</div>
					</div>
					<div class="clearfix">
						<div class="wp_box  col-xs-8">
							<dl>
								<dt>状态：</dt>
								<dd>
		                          	<select class="select" name="status">
		                                <option value="1" <?php if($data['status'] == 1): ?>selected="selected"<?php endif; ?> >启用</option>
		                                <option value="0" <?php if($data['status'] == 0): ?>selected="selected"<?php endif; ?> >禁用</option>
		                            </select>
								</dd>
							</dl>
						</div>
					</div>
					<div class="btnArea">
						<a href="javascript:;" class="btn btn-sereachBg" id="submitBtn">
							<span class="public-label">提交</span>
						</a>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$(function(){
	        $("#submitBtn").on('click',function(){
	        	cvphp.submit($("form"),function(data){
	        		if(data.status!=1){
	        			layer.msg(data.info);
	        		}else{
	        			layer.msg('编辑成功');
	        			setTimeout(function(){
	        				window.location.href="<?php echo U('Admin/index');?>";
	        			},2000);
	        		}
	        	});
	        });
		});
	</script>
</html>