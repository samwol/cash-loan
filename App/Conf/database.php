<?php
    //数据库配置
    return array(
        'DB_TYPE'   => 'mysqli',
        'DB_HOST'   => 'localhost',//数据库连接主机,默认无需修改
        'DB_NAME'   => 'daikuan',//数据库名称
        'DB_USER'   => 'root',//数据库连接用户名
        'DB_PWD'    => 'root',//数据库连接密码
        'DB_PORT'   => 3306,//数据库端口,默认无需修改
        'DB_PREFIX' => 'cv_',//勿修改
    );
