<?php
class PayAction extends CommonAction
{
	public function index()
	{
		$payorderModel = D("Payorder");
		$where = array("pending" => 0);
		if (I("s-oid")) {
			$where["oid"] = I("s-oid");
		}
		if (I("s-status")) {
			$where["status"] = I("s-status");
		}
		import("ORG.Util.Page");
		$count = $payorderModel->where($where)->count();
		$Page = new Page($count, C("PAGE_NUM_ONE"));
		$Page->setConfig("header", "条记录,每页显示" . C("PAGE_NUM_ONE") . "条");
		$Page->setConfig("prev", "<");
		$Page->setConfig("next", ">");
		$Page->setConfig("theme", C("PAGE_STYLE"));
		$show = $Page->show();
		$list = $payorderModel->where($where)->order("add_time Desc")->limit($Page->firstRow . "," . $Page->listRows)->relation(true)->select();
		$this->assign("list", $list);
		$this->assign("page", $show);
		$this->display();
		return NULL;
	}
	public function delOrder()
	{
		$id = I("id");
		if (!$id) {
			$this->error("订单参数有误");
		}
		$payorderModel = D("Payorder");
		$info = $payorderModel->where(array("id" => $id))->find();
		if (!$info) {
			$this->error("该订单不存在");
		}
		$result = $payorderModel->where(array("id" => $id))->delete();
		if (!$result) {
			$this->error("订单操作失败");
		}
		$this->success("删除成功");
	}
}