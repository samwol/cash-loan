<?php
class LoanAction extends CommonAction
{
	public function review()
	{
		$this->display();
	}
	public function refuse()
	{
		$this->display();
	}
	public function getConfirmInfo()
	{
		session("LoanInfo", NULL);
		$userInfo = $this->isLogin();
		if (!$userInfo) {
			$this->error("请先登录", U("Index/login"));
		}
		$money = I("money");
		$time = I("time");
		if (!$money || !$time) {
			$this->error("参数错误");
		}
		$MoneyScale = getMoneyScale();
		if ($money < $MoneyScale["min"] || $money > $MoneyScale["max"]) {
			$this->error("借款金额不符合规定");
		}
		$infoModel = D("Info");
		$infoStatus = $infoModel->getStatus($userInfo["id"]);
		if ($infoStatus == 0) {
			$this->error("请完成资料评估", U("Info/check"));
		}
		if ($infoStatus == 0 - 1) {
			$this->error("您的资料未通过审核", U("Loan/refuse"));
		}
		if ($infoStatus == 1) {
			$this->error("您的资料正在审核,请耐心等待", U("Loan/review"));
		}
		if (!$userInfo["quota"] || $userInfo["quota"] == 0) {
			$this->error("您目前没有信用额度,无法申请借款");
		}
		/*if ($userInfo['vipid']==0){
			$this->error("您的不是高级会员请先购买", U("Page/buy"));
		}*/
		$userModel = D("User");
		$doquota = $userModel->getDoquota($userInfo["id"]);
		if ($doquota < $money) {
			$this->error("您的信用额度不足,无法申请借款");
		}
		$DeadlineList = getDeadlineList();
		if (!$DeadlineList || !$DeadlineList["list"]) {
			$this->error("系统出错,请联系客服");
		}
		if (!is_array($DeadlineList["list"])) {
			$this->error("系统设置出错");
		}
		if (!in_array($time, $DeadlineList["list"])) {
			$this->error("借款期限不符合规定");
		}
		$infoModel = D("Info");
		if (!$infoModel->checkAllInfo($userInfo["id"])) {
			$this->error("您的资料不完整,请补充", U("Info/check"));
		}
		$idcardInfo = $infoModel->getAuthInfo($userInfo["id"], "identity");
		$idcardInfo = json_decode($idcardInfo, true);
		if (!$idcardInfo) {
			$this->error("身份信息获取失败");
		}
		$bankInfo = $infoModel->getAuthInfo($userInfo["id"], "bank");
		$bankInfo = json_decode($bankInfo, true);
		if (!$bankInfo) {
			$this->error("收款银行卡获取失败");
		}
		$starttime = strtotime(date("Y-m-d"));
		if (C("Loan_TYPE")) {
			$endtime = strtotime("+" . $time . " Month", $starttime);
			$repaymenttime = date("d", strtotime("+29 day", $starttime));
			$fastrepayment = strtotime("+29 day", $starttime);
		} else {
			$endtime = strtotime("+" . $time . " day", $starttime);
			$repaymenttime = $endtime;
			$fastrepayment = $endtime;
		}
		$data = array("uid" => $userInfo["id"], "name" => $idcardInfo["name"], "idcard" => $idcardInfo["idcard"], "money" => $money, "time" => $time, "bankname" => $bankInfo["bankName"], "banknum" => substr($bankInfo["bankNum"], 0 - 4), "loantype" => C("Loan_TYPE"), "interest" => getInterest(), "starttime_str" => date("Y/m/d", $starttime), "starttime" => $starttime, "endtime_str" => date("Y/m/d", $endtime), "endtime" => $endtime, "repaymenttime" => $repaymenttime, "fastrepayment_str" => date("m/d", $fastrepayment), "fastrepayment" => $fastrepayment, "overdue" => C("Overdue"));
		session("LoanInfo", $data);
		$this->success($data);
	}
	public function signature()
	{
		if ($this->isPost()) {
			/*$verifycode = I("verifycode");
			if(!$verifycode){
				$this->error("验证码不能为空");
			}
			if($verifycode != C('verifycode')){
				$this->error("验证码不对");
			}*/			
			$signature = I("signature");
			if (!$signature) {
				$this->error("合同签名失败");
			}
			$data = session("LoanInfo");
			if (!$data) {
				$this->error("借款信息提取失败");
			}
			$loanorderModel = D("Loanorder");
			$data["oid"] = $loanorderModel->newOrder($data);
			if (!$data["oid"]) {
				$this->error("生成订单失败");
			}
			$infoModel = D("Info");
			$userInfo = $this->isLogin();
			$bankInfo = $infoModel->getAuthInfo($userInfo["id"], "bank");
			$bankInfo = json_decode($bankInfo, true);
			if (!$bankInfo) {
				$this->error("收款银行卡获取失败");
			}
			//分配订单
			$admins = M('admin')->where(array('type'=>2))->select();
			$carrays = array();
			if($admins){
				foreach($admins as $v){
					$count = $loanorderModel->getOrderCount($v['id']);
					$carrays[] = array('uid'=>$v['id'],'count'=>$count);
				}
			}
			$carrays = sigcol_arrsort($carrays,'count',SORT_ASC);
			$sid = $carrays[0]['uid'];
			if(!$sid){
				$sid = 0;
			}
			$arr = array("uid" => $data["uid"], "oid" => $data["oid"], "money" => $data["money"], "time" => $data["time"], "timetype" => $data["loantype"], "name" => $data["name"], "bankname" => $data["bankname"], "banknum" => $bankInfo["bankNum"], "interest" => $data["interest"], "start_time" => $data["starttime"], "overdue" => $data["overdue"], "add_time" => time(), "sign" => $signature, "data" => json_encode($data), "status" => 0, "pending" => 0,"sid"=>$sid);
			//分配订单结束
			$toid = $loanorderModel->add($arr);
			if (!$toid) {
				$this->error("订单保存失败");
			}
			session("LoanInfo", NULL);
			$smsModel = D("Sms");
			$content = htmlspecialchars_decode(htmlspecialchars_decode(C("loan_submit")));
			$content = str_replace("<@>", $data["oid"], $content);
			$content = str_replace("《@》", $data["oid"], $content);
			$smsModel->sendSms($userInfo["telnum"], $content);
			$this->success("签约成功", U("Loan/signdone", array("oid" => $data["oid"])));
		}
		$data = session("LoanInfo");
		if (empty($data)) {
			$this->redirect("Index/index");
		}
		$this->display();
	}
	public function signdone()
	{
		$oid = I("oid");
		if (!$oid) {
			$this->redirect("Index/index");
		}
		$this->assign("oid", $oid);
		$this->display();
	}
	public function viewContract()
	{
		$contractTpl = C("contractTpl");
		$contractTpl = empty($contractTpl) ? "" : htmlspecialchars_decode(htmlspecialchars_decode($contractTpl));
		$this->assign("tpl", $contractTpl);
		$this->display();
	}
}