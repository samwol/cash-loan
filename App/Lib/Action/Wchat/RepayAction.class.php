<?php
class RepayAction extends CommonAction
{
	public function index()
	{
		$userModel = D("User");
		$userInfo = $this->isLogin();
		$doquota = $userModel->getDoquota($userInfo["id"]);
		$this->assign("doquota", $doquota);
		$this->assign("quota", $userInfo["quota"]);
		$tmpTime = strtotime("+29 day", time());
		$loanbillModel = D("Loanbill");
		$bill = $loanbillModel->where(array("uid" => $userInfo["id"], "repayment_time" => array("ELT", $tmpTime), "status" => array("IN", "0,1")))->order("repayment_time ASC")->find();
		//$this->assign("money", toMoney($bill["money"] + $bill["interest"] + $bill["overdue"]));
		$this->assign("money", toMoney($bill["money"]+ $bill["overdue"]));
		$this->assign("time", $bill["repayment_time"]?$bill["repayment_time"]:strtotime('now'));
		$this->assign("billId", $bill["id"]);
		$this->display();
	}
	public function order()
	{
		$loanorderModel = D("Loanorder");
		$userInfo = $this->isLogin();
		$list = $loanorderModel->getNoneList($userInfo["id"]);
		$loanbillModel = D("Loanbill");
		$hasMoney = $loanbillModel->where(array("uid" => $userInfo["id"], "status" => array("in", "0,1")))->sum("money");
		$hasInterest = $loanbillModel->where(array("uid" => $userInfo["id"], "status" => array("in", "0,1")))->sum("interest");
		$hasOverdue = $loanbillModel->where(array("uid" => $userInfo["id"], "status" => array("in", "0,1")))->sum("overdue");
		//$this->assign("noneMoney", toMoney($hasMoney + $hasInterest + $hasOverdue));
		$this->assign("noneMoney", toMoney($hasMoney + $hasOverdue));
		$this->assign("list", $list);
		$this->display();
	}
	public function viewbill()
	{
		$id = I("oid");
		if (!$id) {
			$this->error("参数有误");
		}
		$loanorderModel = D("Loanorder");
		$order = $loanorderModel->getOrderInfo($id);
		if (!$order) {
			$this->error("获取账单失败");
		}
		if ($order["pending"] != 1) {
			$this->error("订单状态有误");
		}
		$this->assign("data", $order);
		$this->display();
	}
	public function repayment()
	{
		$billId = I("id");
		if (!$billId) {
			$this->error("账单参数有误");
		}
		$payorderModel = D("Payorder");
		$loanbillModel = D("Loanbill");
		$bill = $loanbillModel->where(array("id" => $billId))->find();
		if (!$bill) {
			$this->error("账单不存在");
		}
		if ($bill["status"] == 2 || $bill["status"] == 3) {
			$this->error("当前账单已还款");
		}
		if ($bill["status"] == 4) {
			$this->error("当前账单已失效");
		}
		$userInfo = $this->isLogin();
		//$billMoney = toMoney($bill["money"] + $bill["interest"] + $bill["overdue"]);
		$billMoney = toMoney($bill["money"] + $bill["overdue"]);
		$order = $payorderModel->newOrder($userInfo["id"], $billMoney, array($billId));
		if (!$order) {
			$this->error("支付订单创建失败");
		}
		//var_dump($bill);
		$this->assign("data", $order);
		$this->display();		
		//$this->redirect("Pay/alipay", array("order" => $order));
		exit(0);
	}
	public function repaymentXq()
	{
		$billId = I("id");
		if (!$billId) {
			$this->error("账单参数有误");
		}
		$payorderxqModel = D("Payorderxq");
		$loanbillModel = D("Loanbill");
		$bill = $loanbillModel->where(array("id" => $billId))->find();
		if (!$bill) {
			$this->error("账单不存在");
		}
		if ($bill["status"] == 2 || $bill["status"] == 3) {
			$this->error("当前账单已还款");
		}
		if ($bill["status"] == 4) {
			$this->error("当前账单已失效");
		}
		//var_dump(intval($bill["overdue_xq"]));die;
		if (intval($bill["overdue_xq"]) == 0) {
			
			$this->error("续期费用出错");
		}		
		$userInfo = $this->isLogin();
		//$billMoney = toMoney($bill["money"] + $bill["interest"] + $bill["overdue"]);
		$billMoney = toMoney($bill["overdue_xq"]);
		$order = $payorderxqModel->newOrder($userInfo["id"], $billMoney, array($billId));
		if (!$order) {
			$this->error("支付订单创建失败");
		}
		//var_dump($bill);
		$this->assign("data", $order);
		$this->display();		
		//$this->redirect("Pay/alipay", array("order" => $order));
		exit(0);
	}	
	public function buy()
	{
		$total_fee = I("total_fee");
		if (!$total_fee) {
			$this->error("参数有误");
		}
		$payorderModel = D("Payorder");
		$userInfo = $this->isLogin();
		$billMoney = toMoney($bill["money"] + $bill["interest"] + $bill["overdue"]);
		$order = $payorderModel->newOrder($userInfo["id"], $total_fee);
		if (!$order) {
			$this->error("支付订单创建失败");
		}
		$this->redirect("Pay/alipay", array("order" => $order));
		exit(0);
	}
}