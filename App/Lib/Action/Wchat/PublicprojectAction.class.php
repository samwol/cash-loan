<?php

class PublicprojectAction extends CommonAction
{
    public function index()
    {
        $this->display();
    }

    public function repayment()
    {
        $billId = I("id");
        if (!$billId) {
            $this->error("产品参数有误");
        }
        //		$payorderModel = D("Payorder");
        //		$loanbillModel = D("Loanbill");
        //		$bill = $loanbillModel->where(array("id" => $billId))->find();
        //		if (!$bill) {
        //			$this->error("账单不存在");
        //		}
        //		if ($bill["status"] == 2 || $bill["status"] == 3) {
        //			$this->error("当前账单已还款");
        //		}
        //		if ($bill["status"] == 4) {
        //			$this->error("当前账单已失效");
        //		}
        //		$userInfo = $this->isLogin();
        //		//$billMoney = toMoney($bill["money"] + $bill["interest"] + $bill["overdue"]);
        //		$billMoney = toMoney($bill["money"] + $bill["overdue"]);
        //		$order = $payorderModel->newOrder($userInfo["id"], $billMoney, array($billId));
        //		if (!$order) {
        //			$this->error("支付订单创建失败");
        //		}
        //		//var_dump($bill);
        //		$this->assign("data", $order);

        $productModel = D('product');
        $product = $productModel->where(['id' => $billId])->find();
        if (!$product) {
            $this->error("产品不存在");
        }
        $this->assign("product", $product);
        $this->display();
        //$this->redirect("Pay/alipay", array("order" => $order));
        exit(0);
    }

    public function order()
    {
//		$loanorderModel = D("Loanorder");
//		$userInfo = $this->isLogin();
//		$list = $loanorderModel->getNoneList($userInfo["id"]);
//		$loanbillModel = D("Loanbill");
//		$hasMoney = $loanbillModel->where(array("uid" => $userInfo["id"], "status" => array("in", "0,1")))->sum("money");
//		$hasInterest = $loanbillModel->where(array("uid" => $userInfo["id"], "status" => array("in", "0,1")))->sum("interest");
//		$hasOverdue = $loanbillModel->where(array("uid" => $userInfo["id"], "status" => array("in", "0,1")))->sum("overdue");
//		//$this->assign("noneMoney", toMoney($hasMoney + $hasInterest + $hasOverdue));
//		$this->assign("noneMoney", toMoney($hasMoney + $hasOverdue));
        $productModel = D("product");
        $list = $productModel->order('id Desc')->select();
        $this->assign("list", $list);
        $this->display();
    }
}