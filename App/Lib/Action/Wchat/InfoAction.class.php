<?php
class InfoAction extends CommonAction
{
	//立木征信APPKEY
	private $appkey = "";//这里填写APPKEY
	private	$version = "1.2.0";
	public function check()
	{
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		$uid = $userInfo["id"];
		if (!$infoModel->hasSetIdentity($uid)) {
			$this->redirect("Info/identityAuth");
		}
		if (!$infoModel->hasSetContacts($uid)) {
			$this->redirect("Info/contactsAuth");
		}
		if (!$infoModel->hasSetBank($uid)) {
			$this->redirect("Info/bankAuth");
		}
		if (!$infoModel->hasSetAddess($uid)) {
			$this->redirect("Info/addessAuth");
		}
		if (!$infoModel->hasSetMobile($uid)) {
			$this->redirect("Info/mobileAuth");
		}
		if (!$infoModel->hasSetTaobao($uid)) {
			$this->redirect("Info/taobaoAuth");
		}
		if ($infoModel->getStatus($uid) == 0) {			
			$infoModel->setStatus($userInfo["id"], 1);
		}
		$this->redirect("Index/index");
	}
	public function uploadImg()
	{
		if ($this->isPost()) {
			$fileName = I("fileName");
			if (!$fileName) {
				$this->error("提交参数有误");
			}
			$fileModel = D("File");
			$File = $fileModel->getFile($fileName);
			if (!$File) {
				$this->error("文件上传出错");
			}
			if (!$File["status"]) {
				$this->error($File["error"]);
			}
			$this->success($File["url"]);
		}
		$this->error("非法操作");
	}
	public function before()
	{
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		$uid = $userInfo["id"];
		$infoModel->checkInfo($uid);
		$userAuth = $infoModel->getAuthInfo($uid);
		$this->assign("auth", $userAuth);
	}
	public function identityAuth()
	{
		$this->before();
		$userInfo = $this->isLogin();
		$infoModel = D("Info");
		if ($infoModel->hasSetIdentity($userInfo["id"])) {
			$this->error("身份信息已提交", U("Info/check"));
		}
		if ($this->isPost()) {
			$frontImg = I("front");
			$backImg = I("back");
			$personImg = I("person");
			if (!$frontImg) {
				$this->error("请上传身份证正面照");
			}
			if (!$backImg) {
				$this->error("请上传身份证反面照");
			}
			if (!$personImg) {
				$this->error("请上手持身份证照");
			}			
			$frontSuffix = substr(strrchr($frontImg, "."), 1);
			$backSuffix = substr(strrchr($backImg, "."), 1);
			$personSuffix = substr(strrchr($personImg, "."), 1);
			$suffix = array("jpg", "png", "jpeg", "gif");
			if (!in_array($frontSuffix, $suffix)) {
				$this->error("身份证正面照片类型有误");
			}
			if (!in_array($backSuffix, $suffix)) {
				$this->error("身份证反面照片类型有误");
			}
			if (!in_array($personSuffix, $suffix)) {
				$this->error("手持身份证照片类型有误");
			}			
			$name = I("realName");
			$idcard = strtoupper(I("idCard"));
			if (!$name || !isChineseName($name)) {
				$this->error("请输入真实姓名");
			}
			if (!$idcard || !isIdCard($idcard)) {
				$this->error("请输入规范的身份证号码");
			}


			// 去除立木征信第三方
            /**
			$zhengModel = D('Zheng');
			$result = $zhengModel->ihttpPost(array("method"=>"api.identity.idcheck","apiKey"=>$this->appkey,"version"=>$this->version,"name" => $name, "identityNo" => $idcard));
			
			if (!$result) {
				$this->error("请求失败");
			}
			$arr = json_decode($result, true);

			if (!$arr) {
				$this->error("解析数据失败");
			}
			if ($arr["code"] != '0000') {
				$this->error($arr["msg"]);
			}
			$arr = $arr["data"];
			//if ($arr["status"] == 1) {
			//	$this->error($arr["data"]["msg"]);
			//}
			if ($arr["resultCode"] == "R002") {
				$this->error("身份认证信息不符");
			}
			if ($arr["resultCode"] == "R003") {
				$this->error("身份证号码不存在");
			}
			**/
			$result = $infoModel->setIdentity($userInfo["id"], array("name" => $name, "idcard" => $idcard, "frontimg" => $frontImg, "backimg" => $backImg,"personimg"=>$personImg));
			if (!$result) {
				$this->error("信息保存失败");
			}
			$this->success("保存成功", U("Info/check"));
		}
		$this->display();
	}
	public function contactsAuth()
	{
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if (!$infoModel->hasSetIdentity($userInfo["id"])) {
			$this->redirect("Info/identityAuth");
		}
		if ($infoModel->hasSetContacts($userInfo["id"])) {
			$this->error("联系人信息已提交", U("Info/check"));
		}
		if ($this->isPost()) {
			$zhishuRelation = I("zhishuRelation");
			$zhishuName = I("zhishuName");
			$zhishuPhone = I("zhishuPhone");
			$jinjiRelation = I("jinjiRelation");
			$jinjiName = I("jinjiName");
			$jinjiPhone = I("jinjiPhone");
			if (!$zhishuRelation || !$jinjiRelation) {
				$this->error("请选择联系人关系");
			}
			if (!$zhishuName || !isChineseName($zhishuName)) {
				$this->error("请输入真实姓名");
			}
			if (!$zhishuPhone || !isMobile($zhishuPhone)) {
				$this->error("请输入规范的手机号");
			}
			if (!$jinjiRelation || !$jinjiRelation) {
				$this->error("请选择联系人关系");
			}
			if (!$jinjiName || !isChineseName($jinjiName)) {
				$this->error("请输入真实姓名");
			}
			if (!$jinjiPhone || !isMobile($jinjiPhone)) {
				$this->error("请输入规范的手机号");
			}
			$data = array("zhishuRelation" => $zhishuRelation, "zhishuName" => $zhishuName, "zhishuPhone" => $zhishuPhone, "jinjiRelation" => $jinjiRelation, "jinjiName" => $jinjiName, "jinjiPhone" => $jinjiPhone);
			$result = $infoModel->setContacts($userInfo["id"], $data);
			if (!$result) {
				$this->error("信息保存失败");
			}
			$this->success("保存成功", U("Info/check"));
		}
		$this->display();
	}
	public function bankAuth()
	{
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if (!$infoModel->hasSetContacts($userInfo["id"])) {
			$this->redirect("Info/contactsAuth");
		}
		if ($infoModel->hasSetBank($userInfo["id"])) {
			$this->error("银行卡信息已提交", U("Info/check"));
		}
		if ($this->isPost()) {
			$bankNum = I("bankNum");
			$bankPhone = I("bankPhone");
			$bankName = I("bankName");
			if (!$bankName) {
				$this->error("请输入开户行");
			}			
			if (!$bankNum) {
				$this->error("请输入银行卡号");
			}
			if (!$bankPhone) {
				$this->error("请输入预留手机号");
			}
			if (!isMobile($bankPhone)) {
				$this->error("请输入规范的手机号");
			}
			$idcard = json_decode($infoModel->getAuthInfo($userInfo["id"], "identity"), true);
			if (!$idcard) {
				$this->error("调起数据失败");
			}
			$realname = $idcard["name"];
			$cardno = $idcard["idcard"];

			// 去除立木征信第三方
            /**
			$zhengModel = D('Zheng');
			//$result = curl("http://www.xauguo.cn/Api/Bank/index/", array("realname" => $realname, "cardno" => $cardno, "bankcard" => $bankNum, "mobile" => $bankPhone, "appkey" => C("ugappkey")), 1);
			$result = $zhengModel->ihttpPost(array("method"=>"api.identity.bankcard4check","apiKey"=>$this->appkey,"version"=>$this->version,"bankCardNo"=>$bankNum,"mobileNo"=>$bankPhone,"name" => $realname, "identityNo" => $cardno));
			//var_dump($result);die;
			$arr = json_decode($result, true);
			if (!$arr) {
				$this->error("解析数据失败");
			}
			if ($arr["code"] != '0000') {
				$this->error($arr["msg"]);
			}

			//if ($arr["data"]["status"] == 1) {
			//	$this->error($arr["data"]["msg"]);
			//}

			$arr = $arr["data"];
			if ($arr["resultCode"] == "R002") {
				$this->error("银行卡信息不符");
			}
			if ($arr["resultCode"] == "R003") {
				$this->error("银行卡不存在");
			}
			**/

			//$result = $arr["data"]["result"];
			//$bankName = $result["information"]["bankname"];



			$result = $infoModel->setBank($userInfo["id"], array("bankName" => $bankName, "bankNum" => $bankNum, "bankPhone" => $bankPhone));
			if (!$result) {
				$this->error("信息保存失败");
			}
			$this->success("保存成功", U("Info/check"));
		}
		$this->display();
	}
	public function addessAuth()
	{
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if (!$infoModel->hasSetBank($userInfo["id"])) {
			$this->redirect("Info/bankAuth");
		}
		if ($infoModel->hasSetAddess($userInfo["id"])) {
			$this->error("联系信息已提交", U("Info/check"));
		}
		if ($this->isPost()) {
			$marriage = I("marriage");
			$education = I("education");
			$industry = I("industry");
			$addess = I("addess");
			$addessMore = I("addessMore");
			if (!$marriage) {
				$this->error("请选择婚姻状态");
			}
			if (!$education) {
				$this->error("请选择最高学历");
			}
			if (!$industry) {
				$this->error("请输入您的工作行业");
			}
			if (!$addess) {
				$this->error("请选择居住城市");
			}
			if (!$addessMore) {
				$this->error("请输入居住地址");
			}
			$result = $infoModel->setAddess($userInfo["id"], array("marriage" => $marriage, "education" => $education, "industry" => $industry, "addess" => $addess, "addessMore" => $addessMore));
			if (!$result) {
				$this->error("信息保存失败");
			}
			$this->success("保存成功", U("Info/check"));
		}
		$this->display();
	}
	
	public function next(){
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();		
		if($this->isPost()){
			$name = I('name');
			if($name == 'mobile' || $name == 'taobao'){
				//$infoModel = D("Info");
				if($name == 'mobile'){
					if (!$infoModel->setMobile($userInfo["id"], "next")) {
						$this->error('获取授权失败');
					}else{
						$this->success('获取授权成功');
					}
				}
				if($name == 'taobao'){
					if (!$infoModel->setTaobao($userInfo["id"], "next")) {
						$this->error('获取授权失败');
					}else{
						$this->success('获取授权成功');
					}	
				}
			}else{
				$this->error("参数错误");
			}
		}
	} 	
	public function mobileAuth()
	{
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if (!$infoModel->hasSetAddess($userInfo["id"])) {
			$this->redirect("Info/addessAuth");
		}
		if ($infoModel->hasSetMobile($userInfo["id"])) {
			$this->error("运营商数据已提交", U("Info/check"));
		}
		if ($this->isPost()) {
			//$mobile = I("mobile");
			//$fwpass = I("fwpass");
			//$verifycode = I("verifycode");
			$token = I("token");
			/*if (!$mobile) {
				$this->error("请输入手机号");
			}			
			if (!$fwpass) {
				$this->error("请输入服务密码");
			}
			if (!$verifycode) {
				$this->error("请输入预留验证码");
			}
			if (!isMobile($mobile)) {
				$this->error("请输入规范的手机号");
			}*/
			if(!$token){
				$this->error("请先获取验证码");
			}			
			$zhengModel = D('Zheng');
			
			
			/*$result = $zhengModel->ihttpPost(array("method"=>"api.common.getStatus","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'mobile'));
			$arr = json_decode($result, true);
			if($arr['code'] == '0006'){
				$result = $zhengModel->ihttpPost(array("method"=>"api.common.input","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "input" => $verifycode));
				do{
					$result = $zhengModel->ihttpPost(array("method"=>"api.common.getResult","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'mobile'));
					$arr = json_decode($result, true);
					if($arr['code'] == '0000'){
						break;
					}
				}while(true);
			}*/
			$result = $zhengModel->ihttpPost(array("method"=>"api.common.getResult","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'mobile'));
			$result1 = $zhengModel->ihttpPost(array("method"=>"api.common.getReport","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'mobile'));
			/*do{
				$result = $zhengModel->ihttpPost(array("method"=>"api.common.getStatus","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'mobile'));
				$arr = json_decode($result, true);
				if($arr["code"] == '0010'){
					$result = $zhengModel->ihttpPost(array("method"=>"api.common.getResult","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'mobile'));
					$arr = json_decode($result, true);					
					break;
				}
			}while(true);*/
			//$result = $zhengModel->ihttpPost(array("method"=>"api.common.getResult","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'mobile'));
			$arr1 = json_decode($result, true);
			$arr2 = json_decode($result1, true);
			
			$data = json_encode(array_merge($arr1['data'],$arr2['data']));
			//var_dump($data);die;
			$infoModel = D("Info");
			if (!$infoModel->setMobile($userInfo["id"], $data)) {
				$this->error('获取授权失败');
			}else{
				$this->success('获取授权成功');
			}
			//$this->ajaxReturn($arr,'JSON');
			/*var_dump($result);die;
			$result = $infoModel->setBank($userInfo["id"], array("bankName" => $bankName, "bankNum" => $bankNum, "bankPhone" => $bankPhone));
			if (!$result) {
				$this->error("信息保存失败");
			}
			$this->success("保存成功", U("Info/check"));*/
		}
		$this->display();
		exit(0);
	}
	public function mobile(){
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if ($this->isPost()) {
			$mobile = I("mobile");
			$fwpass = I("fwpass");			
			if (!$mobile) {
				$this->error("请输入手机号");
			}			
			if (!$fwpass) {
				$this->error("请输入服务密码");
			}
			if (!isMobile($mobile)) {
				$this->error("请输入规范的手机号");
			}
			$idcard = json_decode($infoModel->getAuthInfo($userInfo["id"], "identity"), true);
			if (!$idcard) {
				$this->error("调起数据失败");
			}
			$realname = $idcard["name"];
			$cardno = $idcard["idcard"];
            $infoModel = D("Info");
            if($infoModel->setMobile($userInfo["id"], $mobile)){
                $this->success("授权成功");
            }else{
                $this->error("授权失败");
            }
			// 去除请求
			//$callbackurl = "http://" . $_SERVER["SERVER_NAME"] . U("Callback/mobileAuthCallback");			
//			$zhengModel = D('Zheng');
//			$result = $zhengModel->ihttpPost(array("method"=>"api.mobile.get","apiKey"=>$this->appkey,"version"=>$this->version,"isReport"=>"1","identityCardNo"=>$cardno,"identityName"=>$realname,"contentType"=>"busi;net","username" => $mobile, "password" => base64_encode($fwpass)));
//			$arr = json_decode($result, true);
//			$this->ajaxReturn($arr,'JSON');
		}		
	}
	public function status(){
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if ($this->isPost()) {
			$token = I("token");
			$bizType = "mobile";						
			$zhengModel = D('Zheng');
			$result = $zhengModel->ihttpPost(array("method"=>"api.common.getStatus","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => $bizType));
			$arr = json_decode($result, true);
			$this->ajaxReturn($arr,'JSON');
		}		
	}
	public function sendVerify(){
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if ($this->isPost()) {
			$token = I("token");
			$verifycode = I("verifycode");
			//$bizType = I("bizType");						
			$zhengModel = D('Zheng');
			$result = $zhengModel->ihttpPost(array("method"=>"api.common.input","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "input" => $verifycode));
			$arr = json_decode($result, true);
			$this->ajaxReturn($arr,'JSON');
		}		
	}		
	/*public function mobileAuthReturn()
	{
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		$callid = I("callid");
		if ($callid) {
			$infoModel->setMobile($userInfo["id"], $callid);
		}
		if (!$infoModel->hasSetAddess($userInfo["id"])) {
			$this->redirect("Info/addessAuth");
		}
		if (!$infoModel->hasSetMobile($userInfo["id"])) {
			$this->redirect("Info/mobileAuth");
		}
		$this->display();
	}*/
	public function taobaoAuth()
	{
		/*$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if (!$infoModel->hasSetMobile($userInfo["id"])) {
			$this->redirect("Info/mobileAuth");
		}
		if ($infoModel->hasSetTaobao($userInfo["id"])) {
			$this->error("淘宝数据已提交", U("Info/check"));
		}
		$callbackurl = "http://" . $_SERVER["SERVER_NAME"] . U("Callback/taobaoAuthCallback");
		$returnurl = "http://" . $_SERVER["SERVER_NAME"] . U("Info/taobaoAuthReturn");
		$result = curl("http://www.xauguo.cn/Api/Taobao/geturi/", array("callbackurl" => $callbackurl, "returnurl" => $returnurl, "appkey" => C("ugappkey")), 1);
		if (!$result) {
			$this->error("请求失败");
		}
		$arr = json_decode($result, true);
		if (!$arr) {
			$this->error("解析数据失败");
		}
		if ($arr["code"] != 0) {
			$this->error($arr["data"]);
		}
		$callid = $arr["data"]["callid"];
		if (!$callid) {
			$this->error("预请求发起失败");
		}
		$infoauthModel = D("Infoauth");
		if (!$infoauthModel->addAuth("taobao", $userInfo["id"], $callid)) {
			$this->error("授权信息保存失败");
		}
		header("Location: http://www.xauguo.cn/Api/Taobao/index/?callid=" . $callid);
		exit(0);
		return null;*/
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if (!$infoModel->hasSetMobile($userInfo["id"])) {
			$this->redirect("Info/mobileAuth");
		}
		if ($infoModel->hasSetTaobao($userInfo["id"])) {
			$this->error("淘宝数据已提交", U("Info/check"));
		}
		if ($this->isPost()) {
			//$mobile = I("mobile");
			//$fwpass = I("fwpass");
			//$verifycode = I("verifycode");
			$token = I("token");
			if(!$token){
				$this->error("请先获取验证码");
			}			
			$zhengModel = D('Zheng');
			$result = $zhengModel->ihttpPost(array("method"=>"api.common.getResult","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => 'taobao'));
		
			$arr1 = json_decode($result, true);
			//$arr2 = json_decode($result1, true);
			
			$data = json_encode($arr1['data']);
			//var_dump($data);die;
			$infoModel = D("Info");
			if (!$infoModel->setTaobao($userInfo["id"], $data)) {
				$this->error('获取授权失败');
			}else{
				$this->success('获取授权成功');
			}
			//$this->ajaxReturn($arr,'JSON');
			/*var_dump($result);die;
			$result = $infoModel->setBank($userInfo["id"], array("bankName" => $bankName, "bankNum" => $bankNum, "bankPhone" => $bankPhone));
			if (!$result) {
				$this->error("信息保存失败");
			}
			$this->success("保存成功", U("Info/check"));*/
		}
		$this->display();
		exit(0);
	}
	
	public function taobao(){
		$this->before();
		//$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if ($this->isPost()) {
			/*$mobile = I("mobile");
			$fwpass = I("fwpass");			
			if (!$mobile) {
				$this->error("请输入淘宝帐号");
			}			
			if (!$fwpass) {
				$this->error("请输入淘宝密码");
			}*/
			/*$idcard = json_decode($infoModel->getAuthInfo($userInfo["id"], "identity"), true);
			if (!$idcard) {
				$this->error("调起数据失败");
			}
			$realname = $idcard["name"];
			$cardno = $idcard["idcard"];*/
			//$callbackurl = "http://" . $_SERVER["SERVER_NAME"] . U("Callback/mobileAuthCallback");
            /**
            $zhengModel = D('Zheng');
            $result = $zhengModel->ihttpPost(array("method"=>"api.taobao.get","apiKey"=>$this->appkey,"version"=>$this->version,"loginType"=>"qr","username" => $mobile, "password" => base64_encode($fwpass)));
            $arr = json_decode($result, true);
            $this->ajaxReturn('ok','JSON');
             **/
            $this->ajaxReturn('ok','JSON');
        }
    }
	public function taobaostatus(){
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		if ($this->isPost()) {
			$token = I("token");
			$bizType = "taobao";						
			$zhengModel = D('Zheng');
			$result = $zhengModel->ihttpPost(array("method"=>"api.common.getStatus","apiKey"=>$this->appkey,"version"=>$this->version,"token" => $token, "bizType" => $bizType));
			$arr = json_decode($result, true);
			$this->ajaxReturn($arr,'JSON');
		}		
	}

	
	public function taobaoAuthReturn()
	{
		$this->before();
		$infoModel = D("Info");
		$userInfo = $this->isLogin();
		$callid = I("callid");
		if ($callid) {
			$infoModel->setTaobao($userInfo["id"], $callid);
		}
		$this->display();
	}
}