<?php
class LoanbillModel extends RelationModel
{
	protected $_link = array('User' => array('mapping_type' => BELONGS_TO, 'foreign_key' => 'uid', 'mapping_name' => 'user'), 'Loanorder' => array('mapping_type' => BELONGS_TO, 'foreign_key' => 'toid', 'mapping_name' => 'Loanorder'));
	public function getUserOverdueNum($uid)
	{
		$num = $this->where(array("uid" => $uid, "status" => array("IN", "1,3")))->count();
		return $num;
	}
	public function getUserOverdueMoney($uid)
	{
		$money = $this->where(array("uid" => $uid, "status" => array("IN", "1")))->sum("money");
		$interest = $this->where(array("uid" => $uid, "status" => array("IN", "1")))->sum("interest");
		$overdue = $this->where(array("uid" => $uid, "status" => array("IN", "1")))->sum("overdue");
		return $money + $interest;
	}
	public function getUserNotRepayMoney($uid)
	{
		$money = $this->where(array("uid" => $uid, "status" => array("IN", "0,1")))->sum("money");
		$interest = $this->where(array("uid" => $uid, "status" => array("IN", "0,1")))->sum("interest");
		$overdue = $this->where(array("uid" => $uid, "status" => array("IN", "0,1")))->sum("overdue");
		//return $money + $interest + $overdue;
		return $money + $overdue;
	}
}